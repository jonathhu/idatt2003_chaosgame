module edu.ntnu.stud {
  requires javafx.controls;
  requires javafx.graphics;
  requires org.apache.logging.log4j;
  requires org.junit.jupiter.api;
  requires org.junit.platform.commons;
  requires com.jfoenix;

  exports edu.ntnu.stud;
  exports edu.ntnu.stud.model;
  exports edu.ntnu.stud.model.chaos.fractalgen;
  exports edu.ntnu.stud.view to javafx.graphics;

  opens edu.ntnu.stud to javafx.graphics;
  opens edu.ntnu.stud.model to javafx.graphics, org.junit.platform.commons;
  opens edu.ntnu.stud.model.chaos.fractalgen to javafx.graphics, org.junit.platform.commons;
  exports edu.ntnu.stud.utils;
  opens edu.ntnu.stud.utils to javafx.graphics, org.junit.platform.commons;
  exports edu.ntnu.stud.view.components.submenus;
  opens edu.ntnu.stud.view.components.submenus to javafx.graphics, org.junit.platform.commons;
  exports edu.ntnu.stud.model.filehandling;
  opens edu.ntnu.stud.model.filehandling to javafx.graphics, org.junit.platform.commons;
  exports edu.ntnu.stud.model.chaos.transformations;
  opens edu.ntnu.stud.model.chaos.transformations to javafx.graphics, org.junit.platform.commons;
  exports edu.ntnu.stud.model.linalg;
  opens edu.ntnu.stud.model.linalg to javafx.graphics, org.junit.platform.commons;
  exports edu.ntnu.stud.view.views;
  opens edu.ntnu.stud.view.views to javafx.graphics, org.junit.platform.commons;
  exports edu.ntnu.stud.controller;
  opens edu.ntnu.stud.controller to javafx.graphics, org.junit.platform.commons;
  exports edu.ntnu.stud.observer;
  opens edu.ntnu.stud.observer to javafx.graphics, org.junit.platform.commons, org.mockito;
  exports edu.ntnu.stud.view.components.submenus.custom;
  opens edu.ntnu.stud.view.components.submenus.custom to javafx.graphics, org.junit.platform.commons;
}