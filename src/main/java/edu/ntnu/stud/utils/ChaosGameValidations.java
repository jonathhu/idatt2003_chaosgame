package edu.ntnu.stud.utils;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Class for validating input in the Chaos Game view.
 */
public class ChaosGameValidations {

  private static final Logger LOGGER = LogManager.getLogger(ChaosGameValidations.class);

  /**
   * Validates that the input in a TextField is a double value.
   * @param textField The TextField to validate
   * @return A BooleanProperty that is true if the input is a double value, and false otherwise
   */
  public BooleanProperty validateOnlyDoubleInput(TextField textField) {
    BooleanProperty isValid = new SimpleBooleanProperty(
        textField.getText().isEmpty() || textField.getText().matches("-?\\d+(\\.\\d+)?"));
    textField.textProperty().addListener((observable, oldValue, newValue) -> {
      if (!newValue.matches("-?\\d+(\\.\\d+)?")) {
        isValid.set(false);
        LOGGER.error("Invalid input entered into a textfield where only double values are" +
            "permitted");
      } else {
        isValid.set(true);
      }
    });
    return isValid;
  }

  /**
   * Validates the min and max coordinates of the fractal
   * <p>
   *   Checks that MaxX is greater than MinX and that MaxY is greater than MinY
   * </p>
   * @param minXField
   * @param maxXField
   * @param minYField
   * @param maxYField
   * @return A BooleanProperty that is true if the min and max coordinates are valid, and false
   */
  public BooleanProperty validateMinMaxCoords(TextField minXField, TextField maxXField,
      TextField minYField, TextField maxYField) {
    BooleanProperty isValid = new SimpleBooleanProperty(true);

    // Validate that the text in each TextField is a valid double value
    BooleanProperty minXIsValid = validateOnlyDoubleInput(minXField);
    BooleanProperty maxXIsValid = validateOnlyDoubleInput(maxXField);
    BooleanProperty minYIsValid = validateOnlyDoubleInput(minYField);
    BooleanProperty maxYIsValid = validateOnlyDoubleInput(maxYField);

    // Check if MaxX is greater than MinX
    if (!maxXField.getText().isEmpty() && !minXField.getText().isEmpty()
        && Double.parseDouble(maxXField.getText()) >= Double.parseDouble(minXField.getText())) {
      LOGGER.error("MaxX coordinate is less than or equal to MinX coordinate");
      isValid.set(false);
    }

    // Check if MaxY is greater than MinY
    if (!maxYField.getText().isEmpty() && !minYField.getText().isEmpty()
        && Double.parseDouble(maxYField.getText()) <= Double.parseDouble(minYField.getText())) {
      LOGGER.error("MaxY coordinate is less than or equal to MinY coordinate");
      isValid.set(false);
    }

    // Update the overall validity
    isValid.set(minXIsValid.get() && maxXIsValid.get() && minYIsValid.get() && maxYIsValid.get()
        && isValid.get());

    return isValid;
  }

}


