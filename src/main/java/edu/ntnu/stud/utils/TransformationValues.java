package edu.ntnu.stud.utils;


/**
 * Class for storing values of affine transformations and julia transformations.
 */
public class TransformationValues {

  private double[][] matrixValues;
  private double[] vectorValues;

  /**
   * Constructor for TransformationValues with {@code matrixValues} and {@code vectorValues}.
   * <p>Used to store data for Affine transformations</p>
   *
   * @param matrixValues the values of the Affine transformation matrix's in a 2x2 double matrix.
   * @param vectorValues the values of the Affine transformation vector in a 2D double vector or the
   *                     values of the constantPoint of a JuliaTransform .
   */
  public TransformationValues(double[][] matrixValues, double[] vectorValues) {
    this.matrixValues = matrixValues;
    this.vectorValues = vectorValues;
  }

  /**
   * Constructor for TransformationValues with onlu {@code vectorValues}.
   * <p>Used to store data for Julia transforms</p>
   *
   * @param vectorValues the values of the Affine transformation vector in a 2D double vector or the
   *                     values of the constantPoint of a JuliaTransform .
   */

  public TransformationValues(double[] vectorValues) {
    this.vectorValues = vectorValues;
  }


  public double[][] getMatrixValues() {
    return matrixValues;
  }

  public double[] getVectorValues() {
    return vectorValues;
  }
}