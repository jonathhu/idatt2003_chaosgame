package edu.ntnu.stud.view.components;


import edu.ntnu.stud.model.ChaosGame;
import edu.ntnu.stud.observer.ChaosGameObserver;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

/**
 * The FractalCanvas class is responsible for drawing the fractal on the canvas. It observes the
 * ChaosGame class and updates the canvas when the fractal is updated.
 */

public class FractalCanvas implements ChaosGameObserver {

  ChaosGame chaosGame;
  Canvas canvas;
  boolean isDarkMode;

  /**
   * Constructor for the FractalCanvas class.
   *
   * @param chaosGame The ChaosGame object to observe.
   */
  public FractalCanvas(ChaosGame chaosGame) {
    this.chaosGame = chaosGame;
    this.chaosGame.addObserver(this);
    this.canvas = new Canvas(chaosGame.getCanvas().getWidth(), chaosGame.getCanvas().getHeight());
    this.isDarkMode = chaosGame.getCanvas().isDarkMode();
    initiateDarkOrLightCanvas();
  }

  /**
   * Initiates the canvas with a dark or light background.
   */
  private void initiateDarkOrLightCanvas() {
    if (chaosGame.getCanvas().isDarkMode()) {
      canvas.getGraphicsContext2D().setFill(Color.BLACK);
    } else {
      canvas.getGraphicsContext2D().setFill(Color.WHITE);
    }
    canvas.getGraphicsContext2D().fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
  }

  /**
   * The drawing of the fractal that is displayed to the user on the canvas.
   *
   * Uses GraphicsContext to draw the fractal on the canvas based on amount of pixels on the chaoscanvas.
   */
  public void drawFractal() {
    int[][] fractalList = chaosGame.getCanvas().getCanvasArray();

    GraphicsContext gc = canvas.getGraphicsContext2D();
    clearFractal();

    int maxHits = findMaxHits();
    for (int i = 0; i < fractalList.length; i++) {
      for (int j = 0; j < fractalList[i].length; j++) {
        int hits = fractalList[i][j];
        double normalizedHits = (double) hits / (double) maxHits;

        if (hits == 0) {
          if (chaosGame.getCanvas().isDarkMode()) {
            gc.setFill(Color.BLACK);
          } else {
            continue;
          }
        } else if (hits == maxHits) {
          gc.setFill(Color.GREEN);
        } else if (normalizedHits <= 0.05) {
          gc.setFill(Color.ORANGE);
        } else if (normalizedHits <= 0.15) {
          gc.setFill(Color.BLUE);
        } else if (normalizedHits <= 0.25) {
          gc.setFill(Color.RED);
        } else if (normalizedHits <= 0.50) {
          gc.setFill(Color.BROWN);
        } else {
          gc.setFill(Color.PINK);
        }
        gc.fillRect(j, i, 1, 1);
      }
    }
  }

  /**
   * The drawing of the Julia set fractal that is displayed to the user on the canvas.
   * Uses WritableImage and PixelWriter to draw the fractal on the canvas based on amount of pixels on the chaoscanvas.
   */
  public void drawJuliaSetFractal() {
    int[][] fractalData = chaosGame.getCanvas().getCanvasArray();
    WritableImage buffer = new WritableImage(chaosGame.getCanvas().getWidth(),
        chaosGame.getCanvas().getHeight());
    PixelWriter pixelWriter = buffer.getPixelWriter();

    int maxIterations = chaosGame.getMaxIterations();
    int width = (int) buffer.getWidth();
    int height = (int) buffer.getHeight();
    double centerX = width / 2.0;
    double centerY = height / 2.0;
    double maxDistance = Math.sqrt(centerX * centerX + centerY * centerY);

    for (int i = 0; i < fractalData.length; i++) {
      for (int j = 0; j < fractalData[i].length; j++) {
        int iterations = fractalData[i][j];

        if (iterations == maxIterations || iterations == 0) {
          pixelWriter.setColor(j, i, Color.BLACK);
        } else {
          double fraction = (double) iterations / maxIterations;
          double distance = Math.sqrt(
              (j - centerX) * (j - centerX) + (i - centerY) * (i - centerY));
          double distanceFactor = 1.0 - Math.log(1 + distance) / Math.log(1 + maxDistance);

          // Use a combination of sine functions for smooth transitions
          double redComponent = 1 * (1 + Math.sin(5 * Math.PI * fraction + 0.5)) * distanceFactor;
          double greenComponent =
              0.4 * (1 + Math.sin(5 * Math.PI * fraction + 1.5)) * distanceFactor;
          double blueComponent =
              0.4 * (1 + Math.sin(5 * Math.PI * fraction + 2.5)) * distanceFactor;

          // Adjust intensity for a more dynamic range and smoother blend
          int red = (int) (255 * Math.pow(redComponent, 1.8));
          int green = (int) (255 * Math.pow(greenComponent, 1.2));
          int blue = (int) (255 * Math.pow(blueComponent, 1.2));

          // Ensuring the dominant color remains red with a smoother gradient
          red = Math.min(red * 2, 255);
          green = Math.min(green / 4, 255);
          blue = Math.min(blue / 4, 255);

          Color color = Color.rgb(red, green, blue);
          pixelWriter.setColor(j, i, color);
        }
      }
    }

    GraphicsContext gc = canvas.getGraphicsContext2D();
    gc.drawImage(buffer, 0, 0);
  }

  /**
   * Clears the fractal from the canvas.
   */
  private void clearFractal() {
    GraphicsContext gc = canvas.getGraphicsContext2D();
    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    initiateDarkOrLightCanvas();
  }


  public Canvas getCanvas() {
    return canvas;
  }

  /**
   * Finds the maximum amount of hits of any pixel on the canvas.
   * @return The maximum amount of hits.
   */
  private int findMaxHits() {
    int[][] fractalList = chaosGame.getCanvas().getCanvasArray();
    int maxHits = Integer.MIN_VALUE;

    for (int[] row : fractalList) {
      for (int hits : row) {
        if (hits > maxHits) {
          maxHits = hits;
        }
      }
    }

    return maxHits;
  }

  /**
   * Updates the canvas when the fractal is updated.
   */
  @Override
  public void updateCanvas() {
    chaosGame.runSteps();
    drawFractal();
  }

  /**
   * Updates the theme of the canvas.
   */
  @Override
  public void updateTheme() {
    initiateDarkOrLightCanvas();
  }

  /**
   * Updates the canvas to the Julia set fractal.
   */
  @Override
  public void updateCanvasToJuliaSet() {
    chaosGame.runIterations();
    drawJuliaSetFractal();
  }

}
