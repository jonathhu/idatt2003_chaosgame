package edu.ntnu.stud.view.components;

import com.jfoenix.controls.JFXDrawer;
import javafx.scene.Node;
import javafx.scene.control.Button;

public class GameMenuDrawer extends JFXDrawer {
  private Button arrowButton;

  public GameMenuDrawer(Button arrowButton, Node content) {
    this.arrowButton = arrowButton;
    setSidePane(content);
    initializeDrawer();
  }

  private void initializeDrawer() {
    setDefaultDrawerSize(330);
    setOverLayVisible(false);
    setResizableOnDrag(true);
    setDirection(JFXDrawer.DrawerDirection.LEFT);
    open();

    arrowButton.setOnAction(e -> toggleDrawer());
  }

  private void toggleDrawer() {
    if (isOpened()) {
      close();
      arrowButton.setText("→");
    } else {
      open();
      arrowButton.setText("←");
    }
  }
}