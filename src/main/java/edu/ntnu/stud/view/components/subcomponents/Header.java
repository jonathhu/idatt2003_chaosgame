package edu.ntnu.stud.view.components.subcomponents;

import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Header extends Label {

  public Header(String text, String cssClass, double fontSize, String color) {
    super(text);
    getStyleClass().add(cssClass);
    setFont(Font.font("Arial", FontWeight.BOLD, fontSize));
    setStyle("-fx-text-fill: " + color + ";");
  }
}