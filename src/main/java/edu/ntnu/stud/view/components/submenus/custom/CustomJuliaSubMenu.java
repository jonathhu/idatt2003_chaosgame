package edu.ntnu.stud.view.components.submenus.custom;

import edu.ntnu.stud.controller.ChaosGameController;
import edu.ntnu.stud.model.linalg.Complex;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescriptionFactory;
import edu.ntnu.stud.view.components.subcomponents.GameMenu;
import edu.ntnu.stud.view.components.subcomponents.Header;
import edu.ntnu.stud.utils.TransformationValues;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.RadioButton;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Submenu for creating custom Julia fractals.
 */
public class CustomJuliaSubMenu extends CustomSubMenu {

  private VBox runStepsBox;
  private VBox iterationsAndRadiusBox;
  private StackPane slidersStackPane;
  private Label cValueLabel;
  private TextField realPartField;
  private TextField imaginaryPartField;
  private ToggleGroup group;
  private RadioButton juliaRootButton;
  private RadioButton juliaQuadButton;
  public TransformationValues vectorValues;
  private Slider maxIterationsSlider;
  private Slider escapeRadiusSlider;

  public CustomJuliaSubMenu(GameMenu gameMenu, ChaosGameController chaosGameController) {
    super(gameMenu, "Custom Julia", gameMenu::goBack);
    this.chaosGameController = chaosGameController;
    addContent(initializeCustomJuliaSubMenu());
    addFocusLostEventToFields();
  }

  public CustomJuliaSubMenu(GameMenu gameMenu, ChaosGameController chaosGameController,
      TransformationValues transformations) {
    super(gameMenu, "Custom Julia", gameMenu::goBack);
    this.chaosGameController = chaosGameController;
    this.vectorValues = transformations;

    addContent(initializeCustomJuliaSubMenu());
    addFocusLostEventToFields();

    populateTransformationFields();
    populateCoordsField();
  }

  private void populateTransformationFields() {
    TransformationValues values = vectorValues;
    double[] vector = values.getVectorValues();
    realPartField.setText(String.valueOf(vector[0]));
    imaginaryPartField.setText(String.valueOf(vector[1]));
  }

  private void populateCoordsField() {
    minX.setText(
        String.valueOf(chaosGameController.getFractalDescription().getMinCoords().getX0()));
    minY.setText(
        String.valueOf(chaosGameController.getFractalDescription().getMinCoords().getX1()));
    maxX.setText(
        String.valueOf(chaosGameController.getFractalDescription().getMaxCoords().getX0()));
    maxY.setText(
        String.valueOf(chaosGameController.getFractalDescription().getMaxCoords().getX1()));
  }

  private VBox initializeCustomJuliaSubMenu() {
    VBox customJuliaSubMenu = new VBox();
    customJuliaSubMenu.getChildren().addAll(
        createSpacer(25),
        minMaxCoordsBox(),
        createSpacer(15),
        createJuliaTransformContainer(),
        createSpacer(10),
        createErrorLabel()
    );
    group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
          updateButtonStatus();
        }
    );

    realPartField.textProperty().addListener((observable, oldValue, newValue) -> {
      updateCValueLabel();
      updateButtonStatus();
    });

    imaginaryPartField.textProperty().addListener((observable, oldValue, newValue) -> {
      updateCValueLabel();
      updateButtonStatus();
    });

    if (chaosGameController.getChaosGame().getDescription().getTransforms().size() == 1) {
      juliaQuadButton.setSelected(true);
    } else if (chaosGameController.getChaosGame().getDescription().getTransforms().size() > 1) {
      juliaRootButton.setSelected(true);
    }
    customJuliaSubMenu.setSpacing(10);
    customJuliaSubMenu.setAlignment(Pos.CENTER);
    return customJuliaSubMenu;
  }

  private StackPane createSlidersStackPane() {
    slidersStackPane = new StackPane();
    runStepsBox = createRunStepsBox();
    iterationsAndRadiusBox = createIterationsAndRadiusBox();
    slidersStackPane.getChildren().addAll(runStepsBox, iterationsAndRadiusBox);
    return slidersStackPane;
  }

  private VBox createRunStepsBox() {
    runStepsBox = new VBox();
    Label runStepsLabel = new Label("Run Steps");
    runStepsLabel.setStyle("-fx-font-size: 13");
    runStepsBox.getChildren().addAll(runStepsLabel, createRunStepsSlider());
    runStepsBox.setVisible(false); // Initially hidden
    runStepsBox.setAlignment(Pos.CENTER);
    return runStepsBox;
  }

  private Slider createRunStepsSlider() {
    runStepsSlider = new Slider();
    runStepsSlider = new Slider();
    runStepsSlider.setMin(0);
    runStepsSlider.setMax(200000);
    runStepsSlider.setValue(100000);
    runStepsSlider.setShowTickLabels(true);
    runStepsSlider.setShowTickMarks(true);
    runStepsSlider.setMajorTickUnit(50000);
    runStepsSlider.setMinorTickCount(5);
    runStepsSlider.setBlockIncrement(10000);
    runStepsSlider.setVisible(true);
    runStepsSlider.setDisable(true);

    runStepsSlider.setMaxWidth(200);
    runStepsSlider.valueProperty().addListener((observableValue, oldVal, newVal) ->
        chaosGameController.runSteps(newVal.intValue())
    );

    return this.runStepsSlider;
  }

  private VBox createIterationsAndRadiusBox() {
    iterationsAndRadiusBox = new VBox();
    Label maxIterationsLabel = new Label("Max Iterations");
    maxIterationsLabel.setStyle("-fx-font-size: 13");
    Slider maxIterationsSlider = createMaxIterationsSlider();
    Label escapeRadiusLabel = new Label("Escape Radius");
    escapeRadiusLabel.setStyle("-fx-font-size: 13");
    Slider escapeRadiusSlider = createEscapeRadiusSlider();
    iterationsAndRadiusBox.getChildren().addAll(maxIterationsLabel, maxIterationsSlider,
        escapeRadiusLabel, escapeRadiusSlider);
    iterationsAndRadiusBox.setAlignment(Pos.CENTER);
    iterationsAndRadiusBox.setVisible(false);
    return iterationsAndRadiusBox;
  }

  private Slider createMaxIterationsSlider() {
    maxIterationsSlider = new Slider();
    maxIterationsSlider.setMin(0);
    maxIterationsSlider.setMax(512);
    maxIterationsSlider.setValue(chaosGameController.getChaosGame().getMaxIterations());
    maxIterationsSlider.setShowTickLabels(true);
    maxIterationsSlider.setShowTickMarks(true);
    maxIterationsSlider.setMajorTickUnit(128);
    maxIterationsSlider.setMinorTickCount(32);
    maxIterationsSlider.setBlockIncrement(16);
    maxIterationsSlider.setMaxWidth(200);
    maxIterationsSlider.setVisible(true); // Initially hidden
    maxIterationsSlider.setDisable(true);
    maxIterationsSlider.valueProperty().addListener((observableValue, oldVal, newVal) ->
        chaosGameController.getChaosGame().setMaxIterations(newVal.intValue())
    );
    return maxIterationsSlider;
  }


  private Slider createEscapeRadiusSlider() {
    escapeRadiusSlider = new Slider();
    escapeRadiusSlider.setMin(0);
    escapeRadiusSlider.setMax(50);
    escapeRadiusSlider.setValue(chaosGameController.getChaosGame().getEscapeRadius());
    escapeRadiusSlider.setShowTickLabels(true);
    escapeRadiusSlider.setShowTickMarks(true);
    escapeRadiusSlider.setMajorTickUnit(10);
    escapeRadiusSlider.setMinorTickCount(5);
    escapeRadiusSlider.setBlockIncrement(2);
    escapeRadiusSlider.setMaxWidth(200);
    escapeRadiusSlider.setVisible(true);
    escapeRadiusSlider.setDisable(true);
    escapeRadiusSlider.valueProperty().addListener((observableValue, oldVal, newVal) ->
        chaosGameController.getChaosGame().setEscapeRadius(newVal.intValue())
    );
    return escapeRadiusSlider;
  }

  /*
  private void initiateListenerToFields() {
    addValidationListenerToField(minX);
    addValidationListenerToField(minY);
    addValidationListenerToField(maxX);
    addValidationListenerToField(maxY);
    addValidationListenerToField(realPartField);
    addValidationListenerToField(imaginaryPartField);

    realPartField.textProperty().addListener((observable, oldValue, newValue) -> {
      updateCValueLabel();
      updateButtonStatus();
    });
    imaginaryPartField.textProperty().addListener((observable, oldValue, newValue) -> {
      updateCValueLabel();
      updateButtonStatus();
    });
  }

   */

  private VBox createJuliaTransformContainer() {
    VBox juliaBox = new VBox();
    juliaBox.getChildren().addAll(createCustomJuliaHeader(), realPartRow(), imaginaryPartRow(),
        createSpacer(6), createCValsLabel(), createSpacer(6),
        chooseYourTransformation(), createSpacer(7), createSlidersStackPane(),
        createSpacer(5), displayAndSaveButtons());
    juliaBox.setAlignment(Pos.CENTER);
    juliaBox.setSpacing(15);
    return juliaBox;
  }

  private Header createCustomJuliaHeader() {
    return new Header("Choose Your Complex Constant", "header", 15,
        "#000000");
  }

  private HBox realPartRow() {
    HBox realPartRow = new HBox();
    Label realPartLabel = new Label("Real Part              =      ");
    realPartLabel.setStyle("-fx-font-size: 17px");
    realPartField = new TextField();
    realPartField.setPrefWidth(60);
    realPartRow.getChildren().addAll(realPartLabel, realPartField);
    realPartRow.setAlignment(Pos.CENTER);

    return realPartRow;
  }

  private HBox imaginaryPartRow() {
    HBox imaginaryPartRow = new HBox();
    Label imaginaryPartLabel = new Label("Imaginary Part      =      ");
    Label imaginaryPartSymbol = new Label(" i");
    imaginaryPartSymbol.setStyle("-fx-font-size: 15; -fx-font-weight: bold");
    imaginaryPartLabel.setStyle("-fx-font-size: 17px");
    imaginaryPartField = new TextField();
    imaginaryPartField.setPrefWidth(60);
    imaginaryPartRow.getChildren().addAll(imaginaryPartLabel, imaginaryPartField,
        imaginaryPartSymbol);
    imaginaryPartRow.setAlignment(Pos.CENTER);
    return imaginaryPartRow;
  }

  private Label createCValsLabel() {
    cValueLabel = new Label("C      =      [ ]");
    cValueLabel.setStyle("-fx-font-size: 24");

    return cValueLabel;
  }

  private VBox chooseYourTransformation() {
    VBox transformationBox = new VBox();
    Header transformationHeader = new Header("Choose A Transformation",
        "Custom-Julia-Headers", 15,
        "#000000");
    transformationHeader.setPadding(new Insets(0, 41, 0, 0)); // Add 10 units of left padding
    transformationBox.getChildren()
        .addAll(transformationHeader, createSpacer(5), transformationOptions());
    transformationBox.setAlignment(Pos.CENTER);
    transformationBox.setSpacing(10);
    return transformationBox;
  }

  private VBox transformationOptions() {
    Text juliaSquareRoot = new Text("    z    \u2192   \u221A(z - c)");
    HBox juliaSquareBox = new HBox(juliaSquareRoot);
    juliaSquareBox.setAlignment(Pos.CENTER_LEFT);

    Text juliaQuadratic = new Text("    z    \u2192   z");
    Text juliaQuadratic2 = new Text("²");
    juliaQuadratic2.setFont(new Font(15));
    Text juliaQuadratic3 = new Text(" + c");
    HBox juliaQuadratic2Flow = new HBox(juliaQuadratic, juliaQuadratic2, juliaQuadratic3);
    juliaQuadratic2Flow.setSpacing(-4);
    HBox juliaQuadraticBox = new HBox(juliaQuadratic2Flow);
    juliaQuadraticBox.setAlignment(Pos.CENTER_LEFT);

    juliaRootButton = new RadioButton();
    juliaRootButton.setGraphic(juliaSquareRoot);
    juliaQuadButton = new RadioButton();
    juliaQuadButton.setGraphic(juliaQuadratic2Flow);

    group = new ToggleGroup();
    juliaRootButton.setToggleGroup(group);
    juliaQuadButton.setToggleGroup(group);

    juliaQuadButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
      if (newValue) {
        runStepsBox.setVisible(false);
        iterationsAndRadiusBox.setVisible(true);
      }
    });
    juliaRootButton.selectedProperty().addListener((observable, oldValue, newValue) -> {
      if (newValue) {
        runStepsBox.setVisible(true);
        iterationsAndRadiusBox.setVisible(false);
      }
    });

    juliaRootButton.setStyle("-fx-background-color: transparent; -fx-text-fill: #000000;" +
        " -fx-font-size: 21px; -fx-font-weight: bold;");
    juliaQuadButton.setStyle("-fx-background-color: transparent; -fx-text-fill: #000000; " +
        "-fx-font-size: 21px; -fx-font-weight: bold;");

    juliaRootButton.setStyle("-fx-background-color: transparent; -fx-text-fill: #000000; " +
        "-fx-font-size: 21px; -fx-font-weight: bold;");
    juliaQuadButton.setStyle("-fx-background-color: transparent; -fx-text-fill: #000000;" +
        " -fx-font-size: 21px; -fx-font-weight: bold;");

    VBox optionsBox = new VBox(juliaRootButton, juliaQuadButton);
    optionsBox.setAlignment(Pos.CENTER);
    optionsBox.setSpacing(10);
    return optionsBox;
  }


  private void updateCValueLabel() {
    String realPart = realPartField.getText();
    String imaginaryPart = imaginaryPartField.getText();
    cValueLabel.setText("C = [" + realPart + " + " + imaginaryPart + "i]");
  }

  private VBox displayAndSaveButtons() {
    VBox saveButtonBox = new VBox();
    displayButton = createButton("Display", this::displayFractal);
    displayButton.setDisable(true);
    saveButton = createButton("Save", this::saveFractalToFile);
    saveButtonBox.getChildren().addAll(displayButton, saveButton);
    saveButtonBox.setAlignment(Pos.CENTER);
    saveButtonBox.setSpacing(5);

    displayButton.disabledProperty().addListener((observable, oldValue, newValue) -> {
      maxIterationsSlider.setDisable(newValue);
      escapeRadiusSlider.setDisable(newValue);
    });
    return saveButtonBox;
  }


  @Override
  protected void displayFractal() {
    RadioButton selectedRadioButton = (RadioButton) group.getSelectedToggle();
    int steps = chaosGameController.getSteps();

    if (selectedRadioButton == juliaRootButton) {
      saveSquareRootDescription();
      runStepsSlider.setDisable(false);
      chaosGameController.runSteps(steps);
    } else if (selectedRadioButton == juliaQuadButton) {
      saveQuadraticDescription();
      runStepsSlider.setDisable(false);
      chaosGameController.runIterations(chaosGameController.getChaosGame().getMaxIterations());
    } else {
      displayButton.setDisable(true);
    }


  }


  private void saveSquareRootDescription() {
    chaosGameController.setGameDescription(ChaosGameDescriptionFactory
        .createCustomJuliaSquareRoot(new Complex(Double.parseDouble(realPartField.getText()),
                Double.parseDouble(imaginaryPartField.getText())),
            Double.parseDouble(minX.getText()),
            Double.parseDouble(maxX.getText()), Double.parseDouble(minY.getText()),
            Double.parseDouble(maxY.getText())));
  }

  private void saveQuadraticDescription() {
    chaosGameController.setJuliaDescription(ChaosGameDescriptionFactory.createCustomJuliaSet(
        new Complex(Double.parseDouble(realPartField.getText()),
            Double.parseDouble(imaginaryPartField.getText())),
        Double.parseDouble(minX.getText()), Double.parseDouble(maxX.getText()),
        Double.parseDouble(minY.getText()), Double.parseDouble(maxY.getText()))
    );
  }


  @Override
  protected void lockFields() {
    boolean isErrorInMinMax = !chaosGameController.validateMinMaxCoords(minX, maxX, minY, maxY)
        .isEmpty();
    boolean isErrorInRealPart = !chaosGameController.validateOnlyDoubleInput(realPartField)
        .isEmpty();
    boolean isErrorInImaginaryPart = !chaosGameController.validateOnlyDoubleInput(
            imaginaryPartField)
        .isEmpty();

    if (isErrorInMinMax) {
      realPartField.setDisable(true);
      imaginaryPartField.setDisable(true);
      juliaRootButton.setDisable(true);
      juliaQuadButton.setDisable(true);
      saveButton.setDisable(true);
    } else if (isErrorInRealPart || isErrorInImaginaryPart) {
      minX.setDisable(true);
      maxX.setDisable(true);
      minY.setDisable(true);
      maxY.setDisable(true);
      juliaRootButton.setDisable(true);
      juliaQuadButton.setDisable(true);
      saveButton.setDisable(true);
    } else {
      minX.setDisable(false);
      maxX.setDisable(false);
      minY.setDisable(false);
      maxY.setDisable(false);
      realPartField.setDisable(false);
      imaginaryPartField.setDisable(false);
      juliaRootButton.setDisable(false);
      juliaQuadButton.setDisable(false);
      saveButton.setDisable(false);

    }
  }

  @Override
  protected void updateButtonStatus() {
    displayButton.setDisable(!checkFields() || group.getSelectedToggle() == null);
  }

  @Override
  protected void addFocusLostEventToFields() {
    addValidationListenerToField(minX);
    addValidationListenerToField(minY);
    addValidationListenerToField(maxX);
    addValidationListenerToField(maxY);

    addMinMaxValidationListener(minX, maxX, minY, maxY);

    addValidationListenerToField(realPartField);
    addValidationListenerToField(imaginaryPartField);
  }

  @Override
  protected boolean checkFields() {
    if (minX.getText().isEmpty() || minY.getText().isEmpty() || maxX.getText().isEmpty()
        || maxY.getText().isEmpty() || realPartField.getText().isEmpty()
        || imaginaryPartField.getText().isEmpty()) {
      return false;
    } else {
      try {
        Double.parseDouble(minX.getText());
        Double.parseDouble(minY.getText());
        Double.parseDouble(maxX.getText());
        Double.parseDouble(maxY.getText());
        Double.parseDouble(realPartField.getText());
        Double.parseDouble(imaginaryPartField.getText());
      } catch (NumberFormatException e) {
        return false;
      }
      return true;
    }
  }

  @Override
  protected void saveDescription() {
    if (juliaRootButton.isSelected()) {
      saveSquareRootDescription();
    } else if (juliaQuadButton.isSelected()) {
      saveQuadraticDescription();
    }
  }
}

