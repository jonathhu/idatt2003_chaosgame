package edu.ntnu.stud.view.components.submenus;

import edu.ntnu.stud.controller.ChaosGameController;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescriptionFactory;
import edu.ntnu.stud.view.components.subcomponents.GameMenu;
import javafx.scene.layout.VBox;

/**
 * Submenu for selecting predefined fractals to play.
 */
public class PredefinedSubMenu extends SubMenu {

  private final ChaosGameController chaosGameController;

  public PredefinedSubMenu(GameMenu gameMenu, ChaosGameController chaosGameController) {
    super(gameMenu, "Predefined Fractals", gameMenu::goBack);
    this.chaosGameController = chaosGameController;
    addContent(predefinedPlayOptions());
  }

  private VBox predefinedPlayOptions() {
    VBox buttonBox = new VBox();
    buttonBox.setAlignment(javafx.geometry.Pos.CENTER);
    buttonBox.getChildren().addAll(
        createButton("Sierpinski Triangle", this::playSierpinskiTriangle),
        createSpacer(10),
        createButton("Barnsley Fern", this::playBarnsleyFern),
        createSpacer(10),
        createButton("Levy C Curve", this::playLevy),
        createSpacer(10),
        createButton("JuliaLikeAMandel", this::playJuliaSetMandel),
        createSpacer(10),
        createButton("Julia", this::playJuliaSet),
        createSpacer(10));
    return buttonBox;
  }

  private void playSierpinskiTriangle() {
    chaosGameController.setGameDescription(ChaosGameDescriptionFactory.createSierpinskiTriangle());
    this.gameMenu.switchToSubMenu(new FractalSubMenu(gameMenu, chaosGameController));
    runAction();
  }

  private void playBarnsleyFern() {
    chaosGameController.setGameDescription(ChaosGameDescriptionFactory.createBarnsleyFern());
    this.gameMenu.switchToSubMenu(new FractalSubMenu(gameMenu, chaosGameController));
    runAction();
  }

  private void playLevy() {
    chaosGameController.setGameDescription(ChaosGameDescriptionFactory.createLevyCurveAffine());
    this.gameMenu.switchToSubMenu(new FractalSubMenu(gameMenu, chaosGameController));
    runAction();
  }

  private void playJuliaSetMandel() {
    chaosGameController.setJuliaDescription(ChaosGameDescriptionFactory.
        createPreDefinedJuliaDescriptionLikeMandel());
    this.gameMenu.switchToSubMenu(new FractalSubMenu(gameMenu, chaosGameController));
  }

  private void playJuliaSet() {
    chaosGameController.setGameDescription(ChaosGameDescriptionFactory
        .createPreDefinedJuliaDescriptionStandard());
    this.gameMenu.switchToSubMenu(new FractalSubMenu(gameMenu, chaosGameController));
    runAction();
  }

  private void runAction() {
    chaosGameController.runSteps(100000);
  }

}
