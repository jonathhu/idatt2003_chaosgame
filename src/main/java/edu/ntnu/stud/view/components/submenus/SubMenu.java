package edu.ntnu.stud.view.components.submenus;

import edu.ntnu.stud.controller.ChaosGameController;
import edu.ntnu.stud.view.components.subcomponents.GameMenu;
import edu.ntnu.stud.view.components.subcomponents.Header;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * Abstract class for all submenus in the game menu.
 */
public abstract class SubMenu extends VBox {
  protected final GameMenu gameMenu;
  protected final Button backButton;
  protected final Header header;
  protected ChaosGameController chaosGameController;

  /**
   * Creates a new submenu with a header and a back button.
   * @param gameMenu the game menu
   * @param headerText the text to display in the header
   * @param backAction the action to perform when the back button is clicked
   */
  protected SubMenu(GameMenu gameMenu, String headerText,
                    Runnable backAction) {
    this.gameMenu = gameMenu;
    this.header = new Header(headerText, "submenu-header", 25, "black");
    this.backButton = createButton("Back", backAction);
    this.setAlignment(javafx.geometry.Pos.CENTER);
    getChildren().addAll(createSpacer(20),header, createSpacer(20) ,createSpacer(20),backButton);
  }

  /**
   * Creates a new button with the given text and action.
   * @param text the text to display on the button
   * @param action the action to perform when the button is clicked
   * @return the new button
   */
  protected Button createButton(String text, Runnable action) {
    Button button = new Button(text);
    button.setOnAction(e -> {
      try {
        action.run();
      } catch (Exception ex) {
        System.err.println("An error occurred: " + ex.getMessage());
      }
    });
    return button;
  }


  /**
   * Adds the given content to the submenu.
   * @param content the content to add
   */
  protected void addContent(Node content) {
    getChildren().add(getChildren().size() - 2, content);
  }

  /**
   * Creates a new spacer with the given value.
   * @param value the height of the spacer
   * @return the new spacer
   */
  protected Region createSpacer(int value) {
    Region spacer = new Region();
    spacer.setPrefHeight(value);
    return spacer;
  }
}