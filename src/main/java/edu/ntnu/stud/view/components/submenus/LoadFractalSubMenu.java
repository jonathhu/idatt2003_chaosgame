package edu.ntnu.stud.view.components.submenus;
import edu.ntnu.stud.controller.ChaosGameController;
import edu.ntnu.stud.view.components.subcomponents.GameMenu;
import java.io.File;
import javafx.scene.control.Alert;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 * Submenu for loading fractals from file.
 */
public class LoadFractalSubMenu extends SubMenu {

  private ChaosGameController chaosGameController;

  public LoadFractalSubMenu(GameMenu gameMenu, ChaosGameController chaosGameController) {
    super(gameMenu, "Load Fractal from File", gameMenu::goBack);
    this.chaosGameController = chaosGameController;
    addContent(loadFractalOptions());
  }

  private VBox loadFractalOptions() {
    VBox fractalOptions = new VBox();

    fractalOptions.setAlignment(javafx.geometry.Pos.CENTER);
    fractalOptions.getChildren().add(
        createButton("Select File", this::selectFile));

    return fractalOptions;

  }

  private void selectFile() {
    FileChooser fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().add(
        new FileChooser.ExtensionFilter("Text Files", "*.txt"));
    File selectedFile = fileChooser.showOpenDialog(null);

    if (selectedFile != null) {
      boolean isLoaded = chaosGameController.loadFractalFromFile(selectedFile);
      Alert alert = new Alert(isLoaded ? Alert.AlertType.INFORMATION : Alert.AlertType.ERROR);
      alert.setTitle("File Load Status");
      alert.setHeaderText(null);
      alert.setContentText(isLoaded ? "Fractal loaded successfully!" : "Failed to load fractal.");
      alert.showAndWait();

      if (isLoaded) {
        if (chaosGameController.isCurrentDescriptionJuliaSet()) {
          this.gameMenu.switchToSubMenu(new FractalSubMenu(gameMenu, chaosGameController));
          chaosGameController.runIterations(124);

        } else if(!chaosGameController.isCurrentDescriptionJuliaSet()) {
          this.gameMenu.switchToSubMenu(new FractalSubMenu(gameMenu, chaosGameController));
          chaosGameController.runSteps(100000);
      }}} else {
      Alert alert = new Alert(Alert.AlertType.WARNING);
      alert.setTitle("File Selection");
      alert.setHeaderText(null);
      alert.setContentText("No file selected");
      alert.showAndWait();
    }
  }
}


