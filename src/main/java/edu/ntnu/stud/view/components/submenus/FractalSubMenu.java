package edu.ntnu.stud.view.components.submenus;

import edu.ntnu.stud.controller.ChaosGameController;
import edu.ntnu.stud.view.components.subcomponents.GameMenu;
import edu.ntnu.stud.view.components.submenus.custom.CustomAffineSubMenu;
import edu.ntnu.stud.view.components.submenus.custom.CustomJuliaSubMenu;
import java.io.File;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 * Submenu for fractal options like changing number of steps,
 * saving to file, go to editing submenu.
 */
public class FractalSubMenu extends SubMenu {


  public FractalSubMenu(GameMenu gameMenu, ChaosGameController chaosGameController) {
    super(gameMenu, "Fractal", gameMenu::goBack);
    this.chaosGameController = chaosGameController;
    addContent(fractalOptions());

  }

  private VBox fractalOptions() {
    VBox buttonBox = new VBox();
    buttonBox.setAlignment(javafx.geometry.Pos.CENTER);

    HBox saveEditBox = new HBox();
    saveEditBox.setAlignment(javafx.geometry.Pos.CENTER);
    saveEditBox.getChildren().addAll(
        createButton("Save", this::saveFractal),
        createButton("Edit", this::editFractal)
    );

    if(chaosGameController.isCurrentDescriptionJuliaSet()) {
      buttonBox.getChildren().addAll(maxAndEscapeBox(), createSpacer(6), saveEditBox);
    } else {
      buttonBox.getChildren().addAll(createSlider(), createSpacer(6),
          saveEditBox);
    }
    return buttonBox;
  }

  private Slider createSlider() {
    Slider slider = new Slider();
    slider.setMin(0);
    slider.setMax(200000);
    slider.setValue(100000);
    slider.setShowTickLabels(true);
    slider.setShowTickMarks(true);
    slider.setMajorTickUnit(50000);
    slider.setMinorTickCount(5);
    slider.setBlockIncrement(10000);
    slider.setMaxWidth(200);

    slider.valueProperty().addListener((observableValue, oldVal, newVal) ->
        chaosGameController.runSteps(newVal.intValue())
    );
    return slider;
  }

  private VBox maxAndEscapeBox() {
    VBox maxAndEscapeBox = new VBox();
    Label maxIterationsLabel = new Label("Max Iterations");
    Label escapeRadiusLabel = new Label("Escape Radius");
    maxAndEscapeBox.setAlignment(javafx.geometry.Pos.CENTER);
    maxAndEscapeBox.getChildren().addAll(maxIterationsLabel, createMaxIterationsSlider(),
        createSpacer(6),
        escapeRadiusLabel, createEscapeRadiusSlider()
    );;
    return maxAndEscapeBox;
  }
  private Slider createMaxIterationsSlider() {
    Slider maxIterationsSlider = new Slider();
    maxIterationsSlider.setMin(0);
    maxIterationsSlider.setMax(512);
    maxIterationsSlider.setValue(chaosGameController.getChaosGame().getMaxIterations());
    maxIterationsSlider.setShowTickLabels(true);
    maxIterationsSlider.setShowTickMarks(true);
    maxIterationsSlider.setMajorTickUnit(128);
    maxIterationsSlider.setMinorTickCount(32);
    maxIterationsSlider.setBlockIncrement(16);
    maxIterationsSlider.setMaxWidth(200);
    maxIterationsSlider.setVisible(true);
    maxIterationsSlider.valueProperty().addListener((observableValue, oldVal, newVal) ->
        chaosGameController.getChaosGame().setMaxIterations(newVal.intValue())
    );
    return maxIterationsSlider;
  }


  private Slider createEscapeRadiusSlider() {
    Slider escapeRadiusSlider = new Slider();
    escapeRadiusSlider.setMin(0);
    escapeRadiusSlider.setMax(50);
    escapeRadiusSlider.setValue(chaosGameController.getChaosGame().getEscapeRadius());
    escapeRadiusSlider.setShowTickLabels(true);
    escapeRadiusSlider.setShowTickMarks(true);
    escapeRadiusSlider.setMajorTickUnit(10);
    escapeRadiusSlider.setMinorTickCount(5);
    escapeRadiusSlider.setBlockIncrement(2);
    escapeRadiusSlider.setMaxWidth(200);
    escapeRadiusSlider.setVisible(true);
    escapeRadiusSlider.valueProperty().addListener((observableValue, oldVal, newVal) ->
        chaosGameController.getChaosGame().setEscapeRadius(newVal.intValue())
    );
    return escapeRadiusSlider;
  }

  private void saveFractal() {
    FileChooser fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text Files",
        "*.txt"));
    fileChooser.setInitialFileName("fractal.txt");
    File file = fileChooser.showSaveDialog(null);

    if (file != null) {
      boolean isSaved = chaosGameController.saveFractalToFile(file);
      Alert alert = new Alert(isSaved ? Alert.AlertType.INFORMATION : Alert.AlertType.ERROR);
      alert.setTitle("File Save Status");
      alert.setHeaderText(null);
      alert.setContentText(isSaved ? "Fractal saved successfully!" : "Failed to save fractal.");
      alert.showAndWait();
    }
  }

  private void editFractal() {
    String transformType = chaosGameController.getTransformType();
    if ("affine".equals(transformType)) {
      editAffineFractal();
    } else if ("julia".equals(transformType)) {
      editJuliaFractal();
    } else {
      Alert alert = new Alert(Alert.AlertType.ERROR);
      alert.setTitle("Error");
      alert.setHeaderText(null);
      alert.setContentText("Unknown transform type");
      alert.showAndWait();
    }
  }

  private void editAffineFractal() {
    gameMenu.switchToSubMenu(new CustomAffineSubMenu(gameMenu, chaosGameController, chaosGameController.convertAffineDescriptionToViewData()));
  }

  private void editJuliaFractal() {
    gameMenu.switchToSubMenu(new CustomJuliaSubMenu(gameMenu, chaosGameController,
        chaosGameController.convertJuliaDescriptionToViewData()));
  }


}
