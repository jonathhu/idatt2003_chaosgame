package edu.ntnu.stud.view.components.submenus.custom;

import edu.ntnu.stud.controller.ChaosGameController;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescription;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescriptionFactory;
import edu.ntnu.stud.view.components.subcomponents.GameMenu;
import edu.ntnu.stud.utils.TransformationValues;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * The Custom Affine submenu is a submenu that allows the user to create custom affine
 * transformations, and display the fractal with the custom transformations. Also, the user can save
 * the transformations to a file.
 */
public class CustomAffineSubMenu extends CustomSubMenu {


  private int currentTransformationIndex;
  private Label transformationLabel;
  private List<TransformationValues> transformations;

  private TextField[][] matrixFields;
  private TextField vectorFieldX0;
  private TextField vectorFieldX1;


  /**
   * Constructs a Custom Affine submenu. The submenu contains options for the user to create custom
   * affine transformations, and display the fractal with the custom transformations, as well as to
   * save the transformations to a file. The submenu is constructed with a {@link GameMenu} and a
   * {@link ChaosGameController}.
   *
   * @param gameMenu            the game menu to which the submenu belongs.
   * @param chaosGameController the fractal controller used to display the fractal.
   */
  public CustomAffineSubMenu(GameMenu gameMenu, ChaosGameController chaosGameController) {
    super(gameMenu, "Custom Affine", gameMenu::goBack);
    this.chaosGameController = chaosGameController;
    this.transformations = new ArrayList<>();
    this.currentTransformationIndex = 1;
    addContent(initializeCustomAffineSubMenu());
    addFocusLostEventToFields();
  }

  public CustomAffineSubMenu(GameMenu gameMenu, ChaosGameController chaosGameController,
      List<TransformationValues> transformationValues) {
    super(gameMenu, "Custom Affine", gameMenu::goBack);
    this.chaosGameController = chaosGameController;
    this.transformations = transformationValues;
    this.currentTransformationIndex = 1;
    addContent(initializeCustomAffineSubMenu());
    addFocusLostEventToFields();

    populateTransformationsFields();
    populateCoordsFields();
  }

  private void populateTransformationsFields() {
    for (int i = 0; i < transformations.size(); i++) {
      TransformationValues transformation = transformations.get(i);
      double[][] matrixValues = transformation.getMatrixValues();
      double[] vectorValues = transformation.getVectorValues();

      for (int j = 0; j < 2; j++) {
        for (int k = 0; k < 2; k++) {
          matrixFields[j][k].setText(String.valueOf(matrixValues[j][k]));
        }
      }
      if (i == 0) {
        vectorFieldX0.setText(String.valueOf(vectorValues[0]));
        vectorFieldX1.setText(String.valueOf(vectorValues[1]));
      }


    }
  }

  private void populateCoordsFields() {
    minX.setText(
        String.valueOf(chaosGameController.getChaosGame().getDescription().getMinCoords().getX0()));
    minY.setText(
        String.valueOf(chaosGameController.getChaosGame().getDescription().getMinCoords().getX1()));
    maxX.setText(
        String.valueOf(chaosGameController.getChaosGame().getDescription().getMaxCoords().getX0()));
    maxY.setText(
        String.valueOf(chaosGameController.getChaosGame().getDescription().getMaxCoords().getX1()));
  }


  /**
   * Initializes the Custom Affine submenu options. All components of the submenu are initialized
   * and added to a VBox.
   *
   * @return a VBox containing all the components of the Custom Affine submenu.
   */
  private VBox initializeCustomAffineSubMenu() {
    VBox customAffineBox = new VBox();
    this.runStepsSlider = createSlider();
    this.runStepsSlider.setDisable(true);
    customAffineBox.getChildren().addAll(createSpacer(35), minMaxCoordsBox(),
        createSpacer(30),
        prevNextTransformationBox(),
        createSpacer(6),
        transformationBox(),
        createSpacer(6),
        this.runStepsSlider,
        createSpacer(6),
        createErrorLabel(),
        createSpacer(6),
        saveAndDisplayBox());
    customAffineBox.setSpacing(10);
    customAffineBox.setAlignment(Pos.CENTER);

    return customAffineBox;
  }

  /**
   * Initializes the previous and next transformation buttons. When the 'Next' button is clicked,
   * the {@link #nextTransformation()} method is called. When the 'Previous' button is clicked, the
   * initializes the {@link #prevTransformation()} method is called.
   *
   * @return a HBox containing the previous and next transformation buttons.
   */
  private HBox prevNextTransformationBox() {
    Button nextButton = createButton("Next", this::nextTransformation);
    Button prevButton = createButton("Previous", this::prevTransformation);
    HBox transformationNavBox = new HBox(prevButton, nextButton);
    transformationNavBox.setAlignment(Pos.CENTER);
    return transformationNavBox;
  }

  /**
   * Initializes the transformation box. The transformation box contains the transformation label
   * and the transformation data box.
   *
   * @return a VBox containing the transformation label and the transformation data box.
   */
  private VBox transformationBox() {
    this.transformationLabel = new Label("Transformation " + (currentTransformationIndex));
    VBox transformDataBox = transformationDataBox();
    VBox transformationBox = new VBox(transformationLabel, transformDataBox);
    transformationBox.setAlignment(Pos.CENTER);

    return transformationBox;
  }

  /**
   * Initializes the transformation data box. The transformation data box contains the matrix
   * {@link #createMatrixBox()} and vector {@link #createVectorBox()} boxes.
   *
   * @return a VBox containing the matrix and vector boxes.
   */
  private VBox transformationDataBox() {
    VBox transformationDataBox = new VBox();
    transformationDataBox.setAlignment(Pos.CENTER);

    VBox matrixBox = createMatrixBox();
    VBox vectorBox = createVectorBox();

    transformationDataBox.getChildren().addAll(matrixBox, vectorBox);
    return transformationDataBox;
  }

  /**
   * Creates a VBox containing the matrix text fields. The matrix text fields are used to input the
   * transformation matrix values. The matrix text fields are initialized with the
   * {@link #createMatrixGrid()} method.
   *
   * @return a VBox containing the matrix text fields.
   */
  private VBox createMatrixBox() {
    VBox matrixBox = new VBox();
    matrixBox.setAlignment(Pos.CENTER);

    Label leftBracket = new Label("[");
    leftBracket.setStyle("-fx-font-size: 25px");
    Label rightBracket = new Label("]");
    rightBracket.setStyle("-fx-font-size: 25px");

    GridPane matrixWithBrackets = new GridPane();
    matrixWithBrackets.add(leftBracket, 0, 0, 1, 2);

    GridPane matrixGrid = createMatrixGrid();
    matrixWithBrackets.add(matrixGrid, 1, 0);

    matrixWithBrackets.add(rightBracket, 2, 0, 1, 2);

    Label matrixLabel = new Label("Matrix");
    matrixBox.getChildren().addAll(matrixLabel, matrixWithBrackets);

    return matrixBox;
  }

  /**
   * Creates a 2x2 grid containing the matrix text fields. The matrix text fields are used to input
   * the transformation matrix values.
   *
   * @return a 2x2 grid containing the matrix text fields.
   */
  private GridPane createMatrixGrid() {
    GridPane matrixGrid = new GridPane();
    this.matrixFields = new TextField[2][2];
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        matrixFields[i][j] = new TextField();
        matrixGrid.add(matrixFields[i][j], j, i);
      }
    }
    return matrixGrid;
  }

  /**
   * Creates a VBox containing the vector text fields. The vector text fields are used to input the
   * transformation vector values.
   *
   * @return a VBox containing the vector text fields.
   */
  private VBox createVectorBox() {
    VBox vectorBox = new VBox();
    vectorBox.setAlignment(Pos.CENTER);

    this.vectorFieldX0 = new TextField();
    vectorFieldX0.setPrefWidth(80);
    this.vectorFieldX1 = new TextField();
    vectorFieldX1.setPrefWidth(80);

    Label openBracketVector = new Label("[");
    openBracketVector.setStyle("-fx-font-size: 20");
    Label commaVector = new Label(", ");
    Label closeBracketVector = new Label("]");
    closeBracketVector.setStyle("-fx-font-size: 20");

    HBox vectorFields = new HBox(openBracketVector, vectorFieldX0, createSpacer(5),
        commaVector, vectorFieldX1, closeBracketVector);
    vectorFields.setAlignment(Pos.CENTER);

    vectorBox.getChildren().addAll(new Label("Vector"), vectorFields);

    return vectorBox;
  }

  /**
   * Creates a VBox containing the edit, save and display buttons. The edit button is used to edit
   * the current transformation, the save button is used to save the current transformation to a
   * file, and the display button is used to display the fractal.
   *
   * @return a VBox containing the edit, save and display buttons.
   */

  private Slider createSlider() {
    Slider slider = new Slider();
    slider.setMin(0);
    slider.setMax(200000);
    slider.setValue(100000);
    slider.setShowTickLabels(true);
    slider.setShowTickMarks(true);
    slider.setMajorTickUnit(50000);
    slider.setMinorTickCount(5);
    slider.setBlockIncrement(10000);

    slider.setMaxWidth(200);
    slider.valueProperty().addListener((observableValue, oldVal, newVal) ->
        chaosGameController.runSteps(newVal.intValue())
    );

    return slider;
  }

  private VBox saveAndDisplayBox() {

    saveButton = createButton("Save", this::saveFractalToFile);
    displayButton = createButton("Display", this::displayFractal);
    saveButton.setDisable(true);
    displayButton.setDisable(true);
    VBox container = new VBox();
    container.getChildren().addAll(displayButton, saveButton);
    container.setAlignment(Pos.CENTER);
    container.setSpacing(8);
    return container;
  }

  /**
   * Increments the current transformation index by one, and updates the transformation data box
   * with the next transformation values.
   */
  private void nextTransformation() {
    saveCurrentTransformation();
    currentTransformationIndex++;
    updateTransformation();
  }

  /**
   * Decrements the current transformation index by one, and updates the transformation data box
   * with the previous transformation values.
   */
  private void prevTransformation() {
    if (currentTransformationIndex > 1) {
      if (checkFields()) {
        saveCurrentTransformation();
        currentTransformationIndex--;
        updateTransformation();

      } else {
        currentTransformationIndex--;
        updateTransformation();
      }
    }
  }

  /**
   * Updates the transformation data box with the current transformation values.
   */
  private void updateTransformation() {
    transformationLabel.setText("Transformation " + (currentTransformationIndex));
    clearFields();
    if (currentTransformationIndex <= transformations.size()) {
      updateTransformationDataBoxValues(transformations.get(currentTransformationIndex - 1));
    }
  }

  /**
   * Saves the current transformation values to the {@link #transformations} list. If the current
   * transformation index is greater than the size of the transformations list, a new transformation
   * is created and added to the list. Otherwise, the transformation at the current index is updated
   * with the current transformation values.
   */
  private void saveCurrentTransformation() {
    double[][] matrixValues = new double[2][2];
    double[] vectorValues = new double[2];
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        matrixValues[i][j] = Double.parseDouble(matrixFields[i][j].getText());
      }
    }
    vectorValues[0] = Double.parseDouble(vectorFieldX0.getText());
    vectorValues[1] = Double.parseDouble(vectorFieldX1.getText());
    if (currentTransformationIndex > transformations.size()) {
      transformations.add(new TransformationValues(matrixValues, vectorValues));
    } else {
      transformations.set(currentTransformationIndex - 1, new TransformationValues(matrixValues,
          vectorValues));
    }
  }

  /**
   * Clears all fields in the transformation data box.
   */
  private void clearFields() {
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        matrixFields[i][j].clear();
      }
    }
    vectorFieldX0.clear();
    vectorFieldX1.clear();
  }

  /**
   * Saves the current transformation values to the fractal description. The fractal description is
   * created with the {@link ChaosGameDescriptionFactory#(List, double, double, double, double)}
   * method, and the fractal description is set with the
   * {@link ChaosGameController#setGameDescription(ChaosGameDescription)} method.
   * When the fractal description is set, the fractal is displayed with the custom transformations.
   */
  @Override
  protected void saveDescription() {
    if (chaosGameController.getFractalDescription().getProbabilities() != null) {
      saveCurrentTransformation();
      chaosGameController.setGameDescription(ChaosGameDescriptionFactory
          .createCustomAffineSet(transformations,
              Double.parseDouble(minX.getText()), Double.parseDouble(maxX.getText()),
              Double.parseDouble(minY.getText()), Double.parseDouble(maxY.getText()),
              chaosGameController.getFractalDescription().getProbabilities()));
    } else {
      chaosGameController.setGameDescription(ChaosGameDescriptionFactory
          .createCustomAffineSet(transformations,
              Double.parseDouble(minX.getText()), Double.parseDouble(maxX.getText()),
              Double.parseDouble(minY.getText()), Double.parseDouble(maxY.getText()), null));
    }


  }

  /**
   * Adds a focus lost event to all the text fields in the custom affine submenu. Purpose of this
   * method is to allow fields to be checked for user input when the user clicks out of the field.
   */
  @Override
  protected void addFocusLostEventToFields() {

    addValidationListenerToField(minX);
    addValidationListenerToField(minY);

    addValidationListenerToField(maxX);
    addValidationListenerToField(maxY);

    addMinMaxValidationListener(minX, maxX, minY, maxY);

    for (TextField[] row : matrixFields) {
      for (TextField field : row) {
        addValidationListenerToField(field);

      }
    }
    addValidationListenerToField(vectorFieldX0);
    addValidationListenerToField(vectorFieldX1);

  }

  @Override
  protected void lockFields() {
    boolean isErrorInMinMax = !chaosGameController.validateMinMaxCoords(minX, maxX, minY, maxY)
        .isEmpty();
    boolean isErrorInMatrix = false;
    boolean isErrorInVector = !chaosGameController.validateOnlyDoubleInput(vectorFieldX0).isEmpty()
        || !chaosGameController.validateOnlyDoubleInput(vectorFieldX1).isEmpty();

    for (TextField[] row : matrixFields) {
      for (TextField field : row) {
        if (!chaosGameController.validateOnlyDoubleInput(field).isEmpty()) {
          isErrorInMatrix = true;
          break;
        }
      }
    }

    if (isErrorInMinMax) {
      for (TextField[] row : matrixFields) {
        for (TextField field : row) {
          field.setDisable(true);
        }
      }
      vectorFieldX0.setDisable(true);
      vectorFieldX1.setDisable(true);
    } else if (isErrorInMatrix || isErrorInVector) {
      minX.setDisable(true);
      minY.setDisable(true);
      maxX.setDisable(true);
      maxY.setDisable(true);
      for (TextField[] row : matrixFields) {
        for (TextField field : row) {
          field.setDisable(isErrorInVector);
        }
      }
      vectorFieldX0.setDisable(isErrorInMatrix);
      vectorFieldX1.setDisable(isErrorInMatrix);
    } else {
      minX.setDisable(false);
      minY.setDisable(false);
      maxX.setDisable(false);
      maxY.setDisable(false);
      for (TextField[] row : matrixFields) {
        for (TextField field : row) {
          field.setDisable(false);
        }
      }
      vectorFieldX0.setDisable(false);
      vectorFieldX1.setDisable(false);
    }

  }


  /**
   * Checks if all fields are filled with user input. This method is used with the
   * {@link #updateButtonStatus()} method to enable the display button if all fields are
   * filled.
   *
   * @return true if all fields are filled with user input, false otherwise.
   */
  @Override
  protected boolean checkFields() {
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        if (matrixFields[i][j].getText().isEmpty()) {
          return false;
        }
      }
    }
    if (vectorFieldX0.getText().isEmpty() || vectorFieldX1.getText().isEmpty()) {
      return false;
    }

    return !minX.getText().isEmpty() && !minY.getText().isEmpty() && !maxX.getText().isEmpty()
        && !maxY.getText().isEmpty();
  }

  /**
   * Updates the transformation data box with a set of transformation values, determined by the
   * current transformation index.
   *
   * @param data the transformation values to update the transformation data box with.
   */
  private void updateTransformationDataBoxValues(TransformationValues data) {
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        matrixFields[i][j].setText(Double.toString(data.getMatrixValues()[i][j]));
      }
    }
    vectorFieldX0.setText(Double.toString(data.getVectorValues()[0]));
    vectorFieldX1.setText(Double.toString(data.getVectorValues()[1]));
  }

  /**
   * Displays the fractal with the current transformation values. The fractal is displayed with the
   * {@link ChaosGameController#runSteps(int)} method.
   */
  @Override
  protected void displayFractal() {
    saveCurrentTransformation();
    saveDescription();
    runStepsSlider.setDisable(false);
    chaosGameController.runSteps(100000);

  }

}
