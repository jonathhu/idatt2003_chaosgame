package edu.ntnu.stud.view.components.subcomponents;

import edu.ntnu.stud.controller.ChaosGameController;
import edu.ntnu.stud.view.components.submenus.CreateFractalSubMenu;
import edu.ntnu.stud.view.components.submenus.LoadFractalSubMenu;
import edu.ntnu.stud.view.components.submenus.PredefinedSubMenu;
import edu.ntnu.stud.view.components.submenus.SubMenu;
import edu.ntnu.stud.view.listeners.NavigationActionListener;
import java.util.ArrayDeque;
import java.util.Deque;
import javafx.scene.control.Button;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class GameMenu extends VBox {

  private final Header header;
  private final ChaosGameController chaosGameController;
  private NavigationActionListener navigationActionListener;

  private final Deque<SubMenu> submenuStack = new ArrayDeque<>();

  public GameMenu(ChaosGameController chaosGameController) {
    this.chaosGameController = chaosGameController;
    header = new Header("Chaos Game",
        "canvas-menu-header", 35, "black");
    initializeCanvasMenu();
  }

  public void setNavigationActionListener(NavigationActionListener navigationActionListener) {
    this.navigationActionListener = navigationActionListener;
  }

  private void initializeCanvasMenu() {
    VBox buttonBox = new VBox();
    buttonBox.setAlignment(javafx.geometry.Pos.CENTER);
    Region spacer = new Region();
    spacer.setPrefHeight(10);
    buttonBox.setSpacing(20);

    buttonBox.getChildren().addAll(
        spacer,
        header,
        createButton("Predefined Fractals", this::showPredefinedSubMenu),
        createButton("Create Custom Fractal", this::showCustomFractalSubMenu),
        createButton("Load Fractal from File", this::showLoadFractalSubMenu),
        createButton("Back", this::goBackToHomeView));
    getChildren().addAll(buttonBox);
  }

  private void goBackToHomeView() {
    if (navigationActionListener != null) {
      navigationActionListener.navigationButtonClicked("HomeView");
    }
  }

  private Button createButton(String text, Runnable action) {
    Button button = new Button(text);
    button.getStyleClass().add("menu-buttons");
    button.setOnAction(e -> action.run());
    return button;
  }

  private void showPredefinedSubMenu() {
    switchToSubMenu(new PredefinedSubMenu(this, this.chaosGameController));
  }

  private void showCustomFractalSubMenu() {
    switchToSubMenu(new CreateFractalSubMenu(this, this.chaosGameController));
  }

  private void showLoadFractalSubMenu() {
    switchToSubMenu(new LoadFractalSubMenu(this, this.chaosGameController));
  }

  public void switchToSubMenu(SubMenu submenu) {
    if (!getChildren().isEmpty() && getChildren().get(0) instanceof SubMenu) {
      submenuStack.push((SubMenu) getChildren().get(0));
    }
    getChildren().clear();
    getChildren().add(submenu);
  }

  public void goBack() {
    if (!submenuStack.isEmpty()) {
      getChildren().clear();
      getChildren().add(submenuStack.pop());
    } else {
      goBackToMainMenu();
    }
  }

  private void goBackToMainMenu() {
    getChildren().clear();
    initializeCanvasMenu();
  }

}