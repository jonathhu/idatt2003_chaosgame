package edu.ntnu.stud.view.components.submenus.custom;

import edu.ntnu.stud.view.components.subcomponents.GameMenu;
import edu.ntnu.stud.view.components.submenus.SubMenu;
import java.io.File;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

/**
 * Submenu for creating custom fractals or create custom ones based on predefined fractals.
 */

public abstract class CustomSubMenu extends SubMenu {

  protected TextField minX;
  protected TextField minY;
  protected TextField maxX;
  protected TextField maxY;
  protected Button displayButton;
  protected Button saveButton;
  protected Slider runStepsSlider;
  protected Label errorLabel;

  protected CustomSubMenu(GameMenu gameMenu, String headerText, Runnable backAction) {
    super(gameMenu, headerText, backAction);

  }

  /**
   * Initializes the min and max coordinates text fields. The min and max coordinates are used to
   * determine the bounds of the fractal.
   *
   * @return a HBox containing the min and max coordinates text fields.
   */
  protected HBox minMaxCoordsBox() {
    this.minX = minMaxTextField();
    this.minY = minMaxTextField();
    this.maxX = minMaxTextField();
    this.maxY = minMaxTextField();

    HBox minCoordsBox = createCoordsBox(minX, minY);
    VBox minBox = new VBox(new Label("Min Coord"), minCoordsBox);


    HBox maxCoordsBox = createCoordsBox(maxX, maxY);
    maxCoordsBox.setAlignment(Pos.CENTER);
    VBox maxBox = new VBox(new Label("Max Coord"), maxCoordsBox);

    HBox coordsBox = new HBox(minBox, maxBox);
    coordsBox.setAlignment(Pos.CENTER);
    coordsBox.setSpacing(10);

    return coordsBox;
  }

  /**
   * Creates a label to display error messages.
   * @return
   */
  protected Label createErrorLabel() {
    errorLabel = new Label();
    errorLabel.setStyle("-fx-text-fill: red");
    return errorLabel;
  }

  /**
   * Creates a text field for user input.
   *
   * @return a text field for user input.
   */
  protected TextField minMaxTextField() {
    TextField textField = new TextField();
    textField.setPrefWidth(50);
    addValidationListenerToField(textField);
    return textField;
  }

  /**
   * Adds a validation listener to the given text field. The validation listener checks if the user
   * input is a valid double value.
   * @param field
   */
  protected void addValidationListenerToField(TextField field) {
    field.textProperty().addListener((observable, oldValue, newValue) -> {
        updateButtonStatus();
        //updateSliderStatus();
        String validationMessage = chaosGameController.validateOnlyDoubleInput(field);
        if (validationMessage.isEmpty()) {
          errorLabel.setText("");
        } else {
          errorLabel.setText(validationMessage);
        }
        lockFields();
    });
    field.setOnKeyReleased(event -> {
      if(checkFields()) {
        updateButtonStatus();
        //updateSliderStatus();
      }
    });
  }

  protected void addMinMaxValidationListener(TextField minX, TextField maxX, TextField minY, TextField maxY) {
    minX.textProperty().addListener((observable, oldValue, newValue) -> {
      updateButtonStatus();
      //updateSliderStatus();
      String validationMessage = chaosGameController.validateMinMaxCoords(minX, maxX, minY, maxY);
      if (validationMessage.isEmpty()) {
        errorLabel.setText("");
      } else {
        errorLabel.setText(validationMessage);
      }
      lockFields();
    });

    maxX.textProperty().addListener((observable, oldValue, newValue) -> {
      updateButtonStatus();
      //updateSliderStatus();
      String validationMessage = chaosGameController.validateMinMaxCoords(minX, maxX, minY, maxY);
      if (validationMessage.isEmpty()) {
        errorLabel.setText("");
      } else {
        errorLabel.setText(validationMessage);
      }
      lockFields();
    });

    minY.textProperty().addListener((observable, oldValue, newValue) -> {
      updateButtonStatus();
      //updateSliderStatus();
      String validationMessage = chaosGameController.validateMinMaxCoords(minX, maxX, minY, maxY);
      if (validationMessage.isEmpty()) {
        errorLabel.setText("");
      } else {
        errorLabel.setText(validationMessage);
      }
      lockFields();
    });

    maxY.textProperty().addListener((observable, oldValue, newValue) -> {
      updateButtonStatus();
      //updateSliderStatus();
      String validationMessage = chaosGameController.validateMinMaxCoords(minX, maxX, minY, maxY);
      if (validationMessage.isEmpty()) {
        errorLabel.setText("");
      } else {
        errorLabel.setText(validationMessage);
      }
      lockFields();
    });
  }

  protected abstract void lockFields();

  /**
   * Creates a HBox containing the x and y text fields for user input.
   *
   * @param minCoords the x text field.
   * @param maxCoords the y text field.
   * @return a HBox containing the x and y text fields.
   */
  protected  HBox createCoordsBox(TextField minCoords, TextField maxCoords) {
    Label openBracket = new Label("[");
    openBracket.setStyle("-fx-font-size: 20");
    Label comma = new Label(", ");
    Label closeBracket = new Label("]");
    closeBracket.setStyle("-fx-font-size: 20");

    HBox coordsBox = new HBox(openBracket, minCoords, comma, maxCoords, closeBracket);
    coordsBox.setAlignment(Pos.CENTER);

    return coordsBox;
  }

  /**
   * Saves the current transformation values to a file. The user is prompted to choose a file
   * location to save the transformation values to.
   */

  protected void saveFractalToFile() {
    saveDescription();

    FileChooser fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text Files",
        "*.txt"));
    fileChooser.setInitialFileName("fractal.txt");
    File file = fileChooser.showSaveDialog(null);

    if (file != null) {
      boolean isSaved = chaosGameController.saveFractalToFile(file);
      Alert alert = new Alert(isSaved ? Alert.AlertType.INFORMATION : Alert.AlertType.ERROR);
      alert.setTitle("File Save Status");
      alert.setHeaderText(null);
      alert.setContentText(isSaved ? "Fractal saved successfully!" : "Failed to save fractal.");
      alert.showAndWait();
    }
  }

  protected void updateButtonStatus() {
    displayButton.setDisable(!checkFields());
    saveButton.setDisable(!checkFields());
  }

  /*
  protected void updateSliderStatus() {
    runStepsSlider.setDisable(!checkFields());
  }

   */

  protected abstract void addFocusLostEventToFields();

  protected abstract boolean checkFields();

  protected abstract void displayFractal();

  protected abstract void saveDescription();
}
