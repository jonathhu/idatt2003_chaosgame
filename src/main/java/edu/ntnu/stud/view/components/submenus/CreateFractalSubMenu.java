package edu.ntnu.stud.view.components.submenus;

import edu.ntnu.stud.controller.ChaosGameController;
import edu.ntnu.stud.view.components.subcomponents.GameMenu;
import edu.ntnu.stud.view.components.submenus.custom.CustomAffineSubMenu;
import edu.ntnu.stud.view.components.submenus.custom.CustomJuliaSubMenu;
import javafx.scene.layout.VBox;

/**
 * Submenu for creating custom fractals.
 */
public class CreateFractalSubMenu extends SubMenu {


  public CreateFractalSubMenu(GameMenu gameMenu, ChaosGameController chaosGameController) {
    super(gameMenu, "Custom Fractal", gameMenu::goBack);
    this.chaosGameController = chaosGameController;
    addContent(customFractalOptions());
  }

  private VBox customFractalOptions() {
    VBox buttonBox = new VBox();
    buttonBox.setAlignment(javafx.geometry.Pos.CENTER);
    buttonBox.getChildren().addAll(
        createButton("Affine Transforms",
            () -> gameMenu.switchToSubMenu(new CustomAffineSubMenu(gameMenu, chaosGameController))),
        createButton("Julia Transforms",
            () -> gameMenu.switchToSubMenu(new CustomJuliaSubMenu(gameMenu, chaosGameController))));
    return buttonBox;
  }

}
