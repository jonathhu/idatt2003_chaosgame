package edu.ntnu.stud.view;

import edu.ntnu.stud.controller.ChaosGameController;
import edu.ntnu.stud.model.filehandling.ChaosGameFileHandlerStrategy;
import edu.ntnu.stud.model.filehandling.TxtChaosGameFileHandler;
import edu.ntnu.stud.view.components.FractalCanvas;
import edu.ntnu.stud.view.listeners.ViewNavigator;
import edu.ntnu.stud.view.views.PlayView;
import edu.ntnu.stud.view.views.HomeView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Launch
 */
public class LaunchView extends Application {

  private ViewNavigator viewNavigator;
  private ChaosGameController chaosGameController;
  private HomeView homeView;
  private PlayView playView;
  private FractalCanvas fractalCanvas;

  private static int MAIN_STAGE_WIDTH = 1600;
  private static int MAIN_STAGE_HEIGHT = 1000;

  public ChaosGameFileHandlerStrategy strategy;

  public LaunchView() {
    this.strategy = new TxtChaosGameFileHandler();
  }

  public LaunchView(ChaosGameFileHandlerStrategy strategy) {
    this.strategy = strategy;
  }

  /**
   * Start the application
   * @param primaryStage the primary stage
   */
  @Override
  public void start(Stage primaryStage) {
    chaosGameController = new ChaosGameController(MAIN_STAGE_WIDTH, MAIN_STAGE_HEIGHT, this.strategy);
    fractalCanvas = new FractalCanvas(chaosGameController.getChaosGame());
    chaosGameController.addObserver(chaosGameController.getFractalCanvas());

    homeView = new HomeView();
    playView = new PlayView(chaosGameController, primaryStage);
    viewNavigator = new ViewNavigator(primaryStage);

    homeView.navigationActionListener(viewNavigator);
    playView.navigationActionListener(viewNavigator);

    Scene scene = new Scene(homeView, MAIN_STAGE_WIDTH, MAIN_STAGE_HEIGHT);
    primaryStage.setScene(scene);

    viewNavigator.registerView("HomeView", homeView);
    viewNavigator.registerView("PlayView", playView);
    viewNavigator.showView("HomeView");

    primaryStage.setTitle("Chaos Game");
    primaryStage.setResizable(true);
    primaryStage.show();
  }

  /**
   * Launch the application
   * @param args the arguments
   */
  public void launchApp(String[] args) {
    launch(args);
  }
}