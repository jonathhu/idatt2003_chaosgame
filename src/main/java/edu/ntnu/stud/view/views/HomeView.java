package edu.ntnu.stud.view.views;

import edu.ntnu.stud.view.listeners.NavigationActionListener;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class HomeView extends StackPane implements View {

  private final String standardResourcePath = "file:src/main/resources/images/background.jpg";
  private VBox homeMenu;
  private NavigationActionListener navigationActionListener;

  public HomeView() {
    super();
    initialize();
  }

  @Override
  public void navigationActionListener(NavigationActionListener listener) {
    this.navigationActionListener = listener;
  }

  @Override
  public Parent getRoot() {
    return this;
  }

  public void initialize() {
    homeMenu = createHomeMenu();
    this.getChildren().addAll(background(standardResourcePath),
        homePageContent());
  }

  private VBox createHomeMenu() {
    VBox menu = new VBox();
    menu.getChildren().addAll(playButton(), exitButton());
    menu.setSpacing(10);
    menu.setAlignment(Pos.CENTER);
    return menu;
  }

  private Button playButton() {
    Button playButton = new Button("Play");
    playButton.setOnAction(e -> navigationActionListener
        .navigationButtonClicked("PlayView"));
    return playButton;
  }

  private Button exitButton() {
    Button exitButton = new Button("Exit");
    exitButton.setId("dark");
    exitButton.setOnAction(e -> System.exit(0));
    return exitButton;
  }

  private BorderPane homePageContent() {
    BorderPane homePageContent = new BorderPane();
    homePageContent.setCenter(homeMenu);
    return homePageContent;
  }

  private ImageView background(String resourcePath) {
    Image background = new Image(resourcePath );
    return new ImageView(background);
  }

  public VBox getHomeMenu() {
    return homeMenu;
  }

}