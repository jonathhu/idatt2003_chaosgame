package edu.ntnu.stud.view.views;

import edu.ntnu.stud.view.listeners.NavigationActionListener;
import javafx.scene.Parent;

public interface View {
  void navigationActionListener(NavigationActionListener listener);
  Parent getRoot();
}