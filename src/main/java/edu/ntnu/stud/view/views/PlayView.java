package edu.ntnu.stud.view.views;

import edu.ntnu.stud.controller.ChaosGameController;
import edu.ntnu.stud.view.components.GameMenuDrawer;
import edu.ntnu.stud.view.components.subcomponents.GameMenu;
import edu.ntnu.stud.view.listeners.NavigationActionListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;

public class PlayView extends StackPane implements View {


  private static final String INITIAL_ARROW_BUTTON_TEXT = "←";

  private Canvas canvas;
  private Button leftArrowButton;
  private GameMenuDrawer leftDrawer;
  private Button fullScreenButton;
  private Stage stage;
  private final ChaosGameController chaosGameController;
  private NavigationActionListener navigationActionListener;

  public PlayView(ChaosGameController chaosGameController, Stage stage) {
    this.chaosGameController = chaosGameController;
    this.stage = stage;
    initializeCanvas();
    initializeArrowButton();
    initializeLeftDrawer();
    initializeScene();
  }


  @Override
  public void navigationActionListener(NavigationActionListener listener) {
    this.navigationActionListener = listener;
  }

  @Override
  public Parent getRoot() {
    return this;
  }

  private void initializeCanvas() {
    canvas = chaosGameController.getFractalCanvas().getCanvas();
    this.setFocusTraversable(true);
    this.setOnScroll(this::handleScrollEvent);
    addMouseDragHandler();

    stage.widthProperty().addListener((observable, oldValue, newValue) -> {
      // Update the width of the canvas
      canvas.setWidth(newValue.doubleValue());
      chaosGameController.getFractalCanvas().drawFractal();

    });

    stage.heightProperty().addListener((observable, oldValue, newValue) -> {
      canvas.setHeight(newValue.doubleValue());
      chaosGameController.getFractalCanvas().drawFractal();
    });
  }

  public void handleScrollEvent(ScrollEvent scrollEvent) {
    chaosGameController.handleScrollToUpdateZoom(scrollEvent, canvas.getWidth(),
        canvas.getHeight(), canvas.getLayoutX(), canvas.getLayoutY());
    }



  private void addMouseDragHandler() {
    final double[] dragContext = new double[2];

    this.setOnMousePressed(event -> {
      dragContext[0] = event.getX();
      dragContext[1] = event.getY();
    });

    this.setOnMouseDragged(event -> {
      double deltaX = (event.getX() - dragContext[0]) * chaosGameController.getZoomLevel();
      double deltaY = (event.getY() - dragContext[1]) * chaosGameController.getZoomLevel();

      dragContext[0] = event.getX();
      dragContext[1] = event.getY();

      chaosGameController.handleDragToUpdateZoom(deltaX, deltaY, canvas.getWidth(),
          canvas.getHeight());
    });
  }

  private void initializeArrowButton() {
    leftArrowButton = new Button(INITIAL_ARROW_BUTTON_TEXT);
    leftArrowButton.setId("left-arrow-button");
  }

  private void initializeLeftDrawer() {
    GameMenu gameMenu = new GameMenu(chaosGameController);
    gameMenu.setNavigationActionListener(this::navigationButtonClicked);
    leftDrawer = new GameMenuDrawer(leftArrowButton, gameMenu);
  }

  private void navigationButtonClicked(String viewName) {
    if ("HomeView".equals(viewName)) {
      navigationActionListener.navigationButtonClicked("HomeView");
    }

  }

  private void initializeScene() {
    StackPane leftDrawerStackPane = new StackPane();
    VBox leftArrowContainer = new VBox(leftArrowButton);

    leftArrowContainer.setAlignment(Pos.CENTER_LEFT);
    leftArrowContainer.setPickOnBounds(false);

    fullScreenButton = new Button("Toggle Full Screen");
    fullScreenButton.setOnAction(e -> stage.setFullScreen(true));

    Button toggleDarkOrLightCanvas = new Button("Toggle Canvas Mode");
    toggleDarkOrLightCanvas.setOnAction(e -> chaosGameController.getChaosGame().toggleDarkMode());

    leftDrawerStackPane.getChildren().addAll(leftDrawer, leftArrowContainer);
    StackPane.setAlignment(leftDrawerStackPane, Pos.CENTER_LEFT);

    StackPane.setAlignment(toggleDarkOrLightCanvas, Pos.TOP_RIGHT);
    StackPane.setMargin(toggleDarkOrLightCanvas, new Insets(10, 10, 0, 0));

    StackPane.setAlignment(fullScreenButton, Pos.TOP_RIGHT);
    StackPane.setMargin(fullScreenButton, new Insets(40, 10, 0, 0));

    getChildren().addAll(canvas, leftDrawerStackPane, fullScreenButton, toggleDarkOrLightCanvas);
  }
}



