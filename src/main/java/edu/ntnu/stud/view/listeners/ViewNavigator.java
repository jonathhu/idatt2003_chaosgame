package edu.ntnu.stud.view.listeners;

import edu.ntnu.stud.view.views.View;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Map;

/**
 * The class that handles the navigation between views.
 * It implements the {@link NavigationActionListener} interface.
 */
public class ViewNavigator implements NavigationActionListener {

  private final Stage primaryStage;
  private final Map<String, View> views;

  /**
   * Constructor for the ViewNavigator class.
   * @param primaryStage
   */
  public ViewNavigator(Stage primaryStage) {
    this.primaryStage = primaryStage;
    this.views = new HashMap<>();
  }

  /**
   * Method to register a view.
   * @param name The name of the view.
   * @param view The view to register.
   */
  public void registerView(String name, View view) {
    views.put(name, view);
  }

  /**
   * Method to show a view.
   * @param name The name of the view to show.
   */
  public void showView(String name) {
    View view = views.get(name);
    if (view != null) {
      primaryStage.getScene().setRoot(view.getRoot());
    }
  }

  /**
   * Method to handle the navigation button clicks.
   * @param viewName The name of the view to navigate to.
   */
  @Override
  public void navigationButtonClicked(String viewName) {
  showView(viewName);
  }
}