package edu.ntnu.stud.view.listeners;

/**
 * Interface to handle navigation button clicks.
 */
public interface NavigationActionListener {

  void navigationButtonClicked(String viewName);
}

