package edu.ntnu.stud.observer;

/**
 * Interface for the ChaosGameObserver.
 */
public interface ChaosGameObserver {

  /**
   * Updates the canvas.
   */
  void updateCanvas();

  /**
   * Updates the theme.
   */
  void updateTheme();

  /**
   * Updates the canvas to the Julia set.
   */
  void updateCanvasToJuliaSet();
}
