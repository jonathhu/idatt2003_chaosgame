package edu.ntnu.stud.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Subject for the {@link ChaosGameObserver}.
 */
public abstract class ChaosGameSubject {

  List<ChaosGameObserver> observers;

  /**
   * Constructor for the ChaosGameSubject. Initialized empty list of observers.
   */
  public ChaosGameSubject() {
    observers = new ArrayList<>();
  }

  /**
   * Adds an observer to the list of observers.
   *
   * @param observer
   */
  public void addObserver(ChaosGameObserver observer) {
    if (!observers.contains(observer)) {
      observers.add(observer);
    }
  }

  /**
   * Removes an observer from the list of observers.
   *
   * @param observer
   */
  public void removeObserver(ChaosGameObserver observer) {
    if (observers.contains(observer)) {
      observers.remove(observer);
    }
  }

  /**
   * Updates all canvas observers.
   */
  public void updateCanvasObservers() {
    for (ChaosGameObserver observer : observers) {
      observer.updateCanvas();
    }
  }

  /**
   * Updates all theme observers.
   */
  public void updateThemeObservers() {
    for (ChaosGameObserver observer : observers) {
      observer.updateTheme();
    }
  }

  /**
   * Updates all canvas observers to the Julia set.
   */
  public void updateCanvasToJuliaObservers() {
    for (ChaosGameObserver observer : observers) {
      observer.updateCanvasToJuliaSet();
    }
  }

}
