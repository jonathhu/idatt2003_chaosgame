package edu.ntnu.stud.model.linalg;

import java.util.Objects;

/**
 * The Vector2D class represents a vector in two dimensions and provides basic operations to
 * manipulate it
 *
 * @author Jonathan Hübertz, Aryan Malekian
 * @version 0.1
 */
public class Vector2D {

  private final double x0;
  private final double x1;

  /**
   * Constructs a new Vector2D object with the specified coordinates.
   *
   * @param x0 the x-coordinate of the vector
   * @param x1 the y-coordinate of the vector
   */
  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Gets the x-coordinate of the vector.
   *
   * @return the x-coordinate of the vector
   */
  public double getX0() {
    return x0;
  }

  /**
   * Gets the y-coordinate of the vector.
   *
   * @return the y-coordinate of the vector
   */
  public double getX1() {
    return x1;
  }

  /**
   * Performs addition of this vector and another vector.
   *
   * @param other the vector to be added
   * @return the result of adding the other vector to this vector
   */
  public Vector2D add(Vector2D other) {
    double newX0 = this.getX0() + other.getX0();
    double newX1 = this.getX1() + other.getX1();

    return new Vector2D(newX0, newX1);
  }

  /**
   * Performs subtraction of this vector and another vector.
   *
   * @param other the vector to be subtracted
   * @return the result of subtracting the other vector from this vector
   */
  public Vector2D subtract(Vector2D other) {
    double newX0 = this.getX0() - other.getX0();
    double newX1 = this.getX1() - other.getX1();

    return new Vector2D(newX0, newX1);
  }


  /**
   * Checks if vector {@code o} is equal to this instance of {@code Vector2D}.
   *
   * @param o the object to be compared
   * @return true if the vector {@code o} is equal to this instance of {@code Vector2D}
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Vector2D vector2D = (Vector2D) o;
    return Double.compare(getX0(), vector2D.getX0()) == 0
        && Double.compare(getX1(), vector2D.getX1()) == 0;
  }

  /**
   * Returns the hash code of this vector.
   *
   * @return the hash code of this vector
   */
  @Override
  public int hashCode() {
    return Objects.hash(getX0(), getX1());
  }

  @Override
  public String toString() {
    return x0 + ", " + x1;
  }
}
