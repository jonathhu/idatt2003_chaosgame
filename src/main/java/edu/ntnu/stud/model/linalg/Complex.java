package edu.ntnu.stud.model.linalg;

/**
 * The Complex class represents a complex number.
 * <p>
 * A complex number is a number that can be expressed on the form of a + bi. The number a is the
 * real part of the complex number, and b is the imaginary part.
 * </p>
 * <p>
 * This class extends the Vector2D class, and represents a complex number as a 2D vector.
 * </p>
 *
 * @author Jonathan Hübertz, Aryan Malekian
 * @version 0.1
 * @see Vector2D
 */
public class Complex extends Vector2D {

  /**
   * Constructs a new Complex number with the specified real and imaginary parts.
   *
   * @param realPart      The real part of the complex number.
   * @param imaginaryPart The imaginary part of the complex number.
   */
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
  }

  public double getRealPart() {
    return this.getX0();
  }

  public double getImaginaryPart() {
    return this.getX1();
  }

  /**
   * Calculates the complex times root of a complex number.
   *
   * @return The complex times root of the complex number.
   */
  public Complex sqrt() {
    if (this.getX0() < 0 && this.getX1() == 0) {
      return new Complex(0, Math.sqrt(Math.abs(this.getX0())));
    }
    double realPart = Math.sqrt(
        0.5 * (Math.sqrt(Math.pow(this.getX0(), 2) + Math.pow(this.getX1(), 2)) +
            this.getX0()));
    double imaginaryPart = Math.signum(this.getX1()) * Math.sqrt(
        0.5 * (Math.sqrt(Math.pow(this.getX0(), 2) + Math.pow(this.getX1(), 2)) -
            this.getX0()));

    return new Complex(realPart, imaginaryPart);
  }

  /**
   * Performs the times operation on this vector.
   *
   * @param other the vector to be squared
   * @return the result of squaring the vector
   */
  public Complex times(Complex other) {
    double realPart = this.getRealPart() * other.getRealPart() - this.getImaginaryPart()
        * other.getImaginaryPart();
    double imaginaryPart = this.getRealPart() * other.getImaginaryPart() + this.getImaginaryPart()
        * other.getRealPart();

    return new Complex(realPart, imaginaryPart);
  }

  /**
   * Performs the add operation on this vector. Adds the real and imaginary parts of the two complex
   * numbers.
   *
   * @param other the vector to be added to this vector
   * @return the result of adding the two vectors
   */
  @Override
  public Complex add(Vector2D other) {
    return new Complex(this.getX0() + other.getX0(), this.getX1() +
        other.getX1());
  }
}
