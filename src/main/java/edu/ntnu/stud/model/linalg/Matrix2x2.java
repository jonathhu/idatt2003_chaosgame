package edu.ntnu.stud.model.linalg;

import java.util.Objects;

/**
 * The Matrix2x2 class represents a 2x2 matrix.
 * <p>
 * The class provides methods to multiply the matrix with a 2D vector. The matrix is immutable so it
 * cannot be changed after it is created.
 * </p>
 * <p>
 * The matrix is represented as follows:
 * <pre>
 *     | a00 a01 |
 *     | a10 a11 |
 *   </pre>
 * </p>
 *
 * @author Jonathan Hübertz, Aryan Malekian
 * @version 0.1
 */
public class Matrix2x2 {

  private final double a00;
  private final double a01;
  private final double a10;
  private final double a11;

  /**
   * Constructs a new 2x2 matrix with the specified elements.
   *
   * @param a00 the element at (0, 0)
   * @param a01 the element at (0, 1)
   * @param a10 the element at (1, 0)
   * @param a11 the element at (1, 1)
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  public double getA00() {
    return a00;
  }

  public double getA01() {
    return a01;
  }

  public double getA10() {
    return a10;
  }

  public double getA11() {
    return a11;
  }

  /**
   * Multiplies this matrix by a given {@code Vector2D}.
   *
   * @param vector the {@code Vector2D} to be multiplied by this matrix
   * @return a new {@code Vector2D} representing the result of the multiplication
   */
  public Vector2D multiply(Vector2D vector) {
    return new Vector2D(a00 * vector.getX0() + a01 * vector.getX1(),
        a10 * vector.getX0() + a11 * vector.getX1());
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Matrix2x2 that = (Matrix2x2) obj;
    return a00 == that.a00 &&
        a01 == that.a01 &&
        a10 == that.a10 &&
        a11 == that.a11;
  }

  @Override
  public int hashCode() {
    return Objects.hash(a00, a01, a10, a11);
  }

  @Override
  public String toString() {
    return a00 + ", " + a01 + ", " + a10 + ", " + a11;
  }
}
