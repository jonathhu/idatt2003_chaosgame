package edu.ntnu.stud.model.filehandling;

import edu.ntnu.stud.exceptions.EmptyFileException;
import edu.ntnu.stud.exceptions.FileNotFoundCustomException;
import edu.ntnu.stud.exceptions.IncorrectFileFormatException;
import edu.ntnu.stud.exceptions.InvalidTransformTypeException;
import edu.ntnu.stud.model.chaos.transformations.Transform2D;
import edu.ntnu.stud.model.chaos.transformations.AffineTransform2D;
import edu.ntnu.stud.model.linalg.Complex;
import edu.ntnu.stud.model.chaos.transformations.JuliaTransform;
import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Vector2D;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescription;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * The TxtChaosGameFileHandler class is responsible for reading and writing chaos game descriptions
 * from and to text files.
 */
public class TxtChaosGameFileHandler implements ChaosGameFileHandlerStrategy {


  /**
   * Defines the strategy to read from a file text file
   * @param path the path to the text file
   * @return a {@link ChaosGameDescription} from file
   * @throws FileNotFoundCustomException
   * @throws IncorrectFileFormatException
   * @throws EmptyFileException
   * @throws InvalidTransformTypeException
   */
  @Override
  public ChaosGameDescription readFromFile(String path)
      throws FileNotFoundCustomException, IncorrectFileFormatException,
      EmptyFileException, InvalidTransformTypeException {
    ChaosGameDescription descriptionToRead = null;
    File file = new File(path);

    try (Scanner scanner = new Scanner(file).useLocale(Locale.ENGLISH)) {

      // Treat commas, newlines and comments as delimiters
      scanner.useDelimiter(",|\n|#.*");

      while (scanner.hasNext()) {
        String transformType = scanner.next().trim();

        // Read the min and max coordinates
        double minX0 = findNextDouble(scanner);
        double minX1 = findNextDouble(scanner);
        double maxX0 = findNextDouble(scanner);
        double maxX1 = findNextDouble(scanner);

        // Create the min and max vectors
        Vector2D min = new Vector2D(minX0, minX1);
        Vector2D max = new Vector2D(maxX0, maxX1);

        // Read the probabilities and checking if they exist
        List<Integer> probabilities = new ArrayList<>();
        while (scanner.hasNextInt()) {
          probabilities.add(scanner.nextInt());
        }

        // Read the transform type
        if (transformType.equals("Affine2D")) {
          if (probabilities.isEmpty()) {
            descriptionToRead = new ChaosGameDescription(min, max,
                readAffineTransforms(scanner));
          } else {
            descriptionToRead = new ChaosGameDescription(min, max,
                readAffineTransforms(scanner), probabilities);
          }
        } else if (transformType.equals("Julia")) {
          descriptionToRead = new ChaosGameDescription(min, max,
              readJulia(scanner));
        } else if (transformType.equals("JuliaSet")) {
          descriptionToRead = new ChaosGameDescription(min, max,
              readJuliaSet(scanner));
        } else {
          throw new InvalidTransformTypeException(
              "No valid transform type found. Found: " + transformType);
        }
      }
    } catch (FileNotFoundException e) {
      throw new FileNotFoundCustomException(path);
    } catch (NoSuchElementException e) {
      throw new IncorrectFileFormatException(path);
    }
    if (descriptionToRead == null) {
      throw new EmptyFileException(path);
    }
    return descriptionToRead;
  }

  /**
   * Defines the strategy to write to a text file
   * @param description the description to be written
   * @param path the path to the text file
   * @throws IOException
   */
  @Override
  public void writeToFile(ChaosGameDescription description, String path)
      throws IOException {

    try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
      // Write the transform type
      if (description.getTransforms().get(0) instanceof AffineTransform2D) {
        writer.write("Affine2D\n");

        // Write the min and max coordinates
        writer.write(
            description.getMinCoords().getX0() + ", " +
                description.getMinCoords().getX1() + "\n");
        writer.write(
            description.getMaxCoords().getX0() + ", " +
                description.getMaxCoords().getX1() + "\n");

        // Write the probabilities
        if (description.getProbabilities() != null) {
          for (Integer probability : description.getProbabilities()) {
            writer.write(probability + "\n");
          }
        }


        // Write the transforms
        for (Transform2D transform : description.getTransforms()) {
          writer.write(transform.toString() + "\n");
        }

      } else if (description.getTransforms().get(0) instanceof JuliaTransform) {
        if (description.getTransforms().size() > 1) {
          writer.write("Julia\n");
        } else {
          writer.write("JuliaSet\n");
        }

        // Write the min and max coordinates
        writer.write(
            description.getMinCoords().getX0() + ", " +
                description.getMinCoords().getX1() + "\n");
        writer.write(
            description.getMaxCoords().getX0() + ", " +
                description.getMaxCoords().getX1() + "\n");

        writer.write(description.getTransforms().get(0).toString());
      }
      else {
        throw new InvalidTransformTypeException(
            description.getTransforms().get(0).getClass().getName());
      }
    }
  }

  /**
   * Reads affine transforms from the scanner
   * @param scanner the scanner
   * @return a list of affine transforms
   */
  private List<Transform2D> readAffineTransforms(Scanner scanner) {
    List<Transform2D> affineTransforms = new ArrayList<>();
    while (scanner.hasNext()) {
      double a00 = findNextDouble(scanner);
      double a01 = findNextDouble(scanner);
      double a10 = findNextDouble(scanner);
      double a11 = findNextDouble(scanner);
      double b0 = findNextDouble(scanner);
      double b1 = findNextDouble(scanner);
      Matrix2x2 matrix = new Matrix2x2(a00, a01, a10, a11);
      Vector2D vector = new Vector2D(b0, b1);
      affineTransforms.add(new AffineTransform2D(matrix, vector));
    }
    return affineTransforms;
  }

  /**
   * Reads Julia transforms from the scanner
   * @param scanner the scanner
   * @return a list of Julia transforms
   */
  private List<Transform2D> readJulia(Scanner scanner) {
    List<Transform2D> juliaTransforms = new ArrayList<>();
    while (scanner.hasNext()) {
      Complex complexPoint = new Complex(findNextDouble(scanner),
          findNextDouble(scanner));
      juliaTransforms.add(new JuliaTransform(complexPoint, 1));
      juliaTransforms.add(new JuliaTransform(complexPoint, -1));
    }
    return juliaTransforms;
  }

  /**
   * Reads Julia set transforms from the scanner
   * @param scanner the scanner
   * @return a list of Julia set transforms
   */
  private List<Transform2D> readJuliaSet(Scanner scanner) {
    List<Transform2D> juliaTransforms = new ArrayList<>();
    while (scanner.hasNext()) {
      Complex complexPoint = new Complex(findNextDouble(scanner),
          findNextDouble(scanner));
      juliaTransforms.add(new JuliaTransform(complexPoint, 1));
    }
    return juliaTransforms;
  }

  /**
   * Finds the next double in the scanner
   * @param scanner the scanner
   * @return the next double
   */
  private double findNextDouble(Scanner scanner) {
    String nextToken;
    do {
      nextToken = scanner.next();
    } while (nextToken.isBlank());
    return Double.parseDouble(nextToken);
  }
}
