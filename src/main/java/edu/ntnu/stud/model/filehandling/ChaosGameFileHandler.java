package edu.ntnu.stud.model.filehandling;

import java.io.IOException;
import edu.ntnu.stud.exceptions.EmptyFileException;
import edu.ntnu.stud.exceptions.FileNotFoundCustomException;
import edu.ntnu.stud.exceptions.IncorrectFileFormatException;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescription;
import edu.ntnu.stud.exceptions.InvalidTransformTypeException;

/**
 * The ChaosGameFileHandler class is responsible for reading and writing chaos game descriptions
 */
public class ChaosGameFileHandler {

  private ChaosGameFileHandlerStrategy strategy;

  /**
   * Constructs a {@link ChaosGameFileHandler} object
   *
   * @param strategy the strategy to be used for reading and writing files
   */

  public ChaosGameFileHandler(ChaosGameFileHandlerStrategy strategy) {
    this.strategy = strategy;
  }

  /**
   * Reads a chaos game description from a file
   *
   * @param path the path to the file
   * @return a {@link ChaosGameDescription} from file
   * @throws FileNotFoundCustomException
   * @throws IncorrectFileFormatException
   * @throws EmptyFileException
   * @throws InvalidTransformTypeException
   */
  public ChaosGameDescription readFromFile(String path)
      throws FileNotFoundCustomException, IncorrectFileFormatException,
      EmptyFileException, InvalidTransformTypeException {
    return strategy.readFromFile(path);
  }

  /**
   * Writes a {@link ChaosGameDescription} to a file
   *
   * @param description the description to be written
   * @param path        the path to the file
   * @throws IOException
   */
  public void writeToFile(ChaosGameDescription description, String path) throws IOException {
    strategy.writeToFile(description, path);
  }

  public void setStrategy(ChaosGameFileHandlerStrategy strategy) {
    this.strategy = strategy;
  }

}


