package edu.ntnu.stud.model.filehandling;

import edu.ntnu.stud.exceptions.EmptyFileException;
import edu.ntnu.stud.exceptions.FileNotFoundCustomException;
import edu.ntnu.stud.exceptions.IncorrectFileFormatException;
import edu.ntnu.stud.exceptions.InvalidTransformTypeException;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescription;
import java.io.IOException;

/**
 * The ChaosGameFileHandlerStrategy interface represents a strategy for reading and writing chaos
 * game descriptions
 */
public interface ChaosGameFileHandlerStrategy {

  /**
   * Reads a chaos game description from a file
   *
   * @param path the path to the file
   * @return a {@link ChaosGameDescription} from file
   * @throws FileNotFoundCustomException
   * @throws IncorrectFileFormatException
   * @throws EmptyFileException
   * @throws InvalidTransformTypeException
   */
  ChaosGameDescription readFromFile(String path)
      throws FileNotFoundCustomException, IncorrectFileFormatException, EmptyFileException, InvalidTransformTypeException;

  /**
   * Writes a {@link ChaosGameDescription} to a file
   *
   * @param description the description to be written
   * @param path        the path to the file
   * @throws IOException
   */
  void writeToFile(ChaosGameDescription description, String path) throws IOException;
}