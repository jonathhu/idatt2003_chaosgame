package edu.ntnu.stud.model.chaos.transformations;

import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Vector2D;
import java.util.Objects;

/**
 * The AffineTransform2D class represents a 2D affine transformation.
 *
 * @author Aryan Malekian
 * @version 0.1
 * @see Transform2D
 * @see Matrix2x2
 */
public class AffineTransform2D implements Transform2D {

  private final Matrix2x2 matrix;
  private final Vector2D vector;

  /**
   * Constructor for AffineTransform2D. Takes a {@link Matrix2x2} and a {@link Vector2D} as input
   * and creates a new AffineTransform2D object.
   *
   * @param matrix 2x2 matrix of type Matrix2x2
   * @param vector 2D vector of type Vector2D
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    this.matrix = matrix;
    this.vector = vector;
  }

  public Matrix2x2 getMatrix() {
    return matrix;
  }

  public Vector2D getVector() {
    return vector;
  }

  /**
   * Transforms a 2D point using the AffineTransform2D object.
   *
   * @param point 2D point of type Vector2D
   * @return a new Vector2D representing the result of the transformation
   */
  public Vector2D transform(Vector2D point) {
    return matrix.multiply(point).add(vector);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    AffineTransform2D that = (AffineTransform2D) obj;
    return Objects.equals(matrix, that.matrix) &&
        Objects.equals(vector, that.vector);
  }

  @Override
  public int hashCode() {
    return Objects.hash(matrix, vector);
  }

  @Override
  public String toString() {
    return this.matrix.toString() + ", " + this.vector.toString();
  }
}
