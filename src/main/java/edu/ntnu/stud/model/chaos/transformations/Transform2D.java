package edu.ntnu.stud.model.chaos.transformations;

import edu.ntnu.stud.model.linalg.Vector2D;

/**
 * The Transform2D interface represents a 2D transformation.
 *
 * @author Jonathan Hübertz, Aryan Malekian
 * @version 0.1
 */
public interface Transform2D {

  /**
   * Performs a transformation on the specified 2D vector.
   *
   * @param point the 2D vector to be transformed
   * @return the transformed vector
   */
  Vector2D transform(Vector2D point);

}
