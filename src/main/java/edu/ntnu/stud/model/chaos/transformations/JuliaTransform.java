package edu.ntnu.stud.model.chaos.transformations;

import edu.ntnu.stud.model.linalg.Complex;
import edu.ntnu.stud.model.linalg.Vector2D;

/**
 * The JuliaTransform class represents a transformation that is used in the Julia set.
 *
 * <p>
 * This class implements the {@link Transform2D} interface. The JuliaTransform class provides a
 * method to transform a {@link Vector2D} using a Julia set formula.
 * </p>
 *
 * @author Jonathan Hübertz
 * @version 0.1
 * @see Transform2D
 * @see Vector2D
 */
public class JuliaTransform implements Transform2D {

  private final Complex constantPoint;
  private final int sign;

  /**
   * Constructs a new JuliaTransform object with the specified complex point and sign.
   *
   * @param point the complex point to subtract from during the transformation
   * @param sign  the sign to apply to the transformed point, either 1 or -1
   */
  public JuliaTransform(Complex point, int sign) {
    this.constantPoint = point;
    this.sign = sign;
  }

  public Complex getConstantPoint() {
    return constantPoint;
  }

  /**
   * Transforms the specified {@code Vector2D} using the Julia set formula.
   *
   * @param point the {@code Vector2D} to be transformed
   * @return the transformed {@code Complex} object
   */
  @Override
  public Complex transform(Vector2D point) {

    Complex newComplex;

    Vector2D subtracted = point.subtract(constantPoint);
    newComplex = new Complex(subtracted.getX0(), subtracted.getX1()).sqrt();
    newComplex = new Complex(sign * newComplex.getX0(),
        sign * newComplex.getX1());

    return newComplex;
  }


  public int getSign() {
    return sign;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    JuliaTransform that = (JuliaTransform) obj;
    return sign == that.sign && constantPoint.equals(that.constantPoint);
  }

  @Override
  public int hashCode() {
    return constantPoint.hashCode() + sign;
  }

  @Override
  public String toString() {
    return constantPoint.toString();
  }

}


