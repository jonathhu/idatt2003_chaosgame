package edu.ntnu.stud.model.chaos.fractalgen;

import edu.ntnu.stud.model.chaos.transformations.AffineTransform2D;
import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Vector2D;
import java.util.Arrays;

/**
 * The ChaosCanvas class represents a canvas for the chaos game.
 *
 * @author Jonathan
 * @version 0.1
 */
public class ChaosCanvas {

  private final int[][] canvas;
  private final int width;
  private final int height;
  private boolean isDarkMode;
  private final Vector2D minCoords;
  private final Vector2D maxCoords;


  private final AffineTransform2D transformCoordsToIndices;

  /**
   * Constructs a new {@code ChaosCanvas} with the specified width, height, and coordinate range.
   *
   * @param width     the width of the canvas
   * @param height    the height of the canvas
   * @param minCoords the minimum coordinates of the fractal in the plane
   * @param maxCoords the maximum coordinates of the fractal in the plane
   */
  public ChaosCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    checkMinMaxCoords(minCoords, maxCoords);
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.canvas = new int[height][width];
    this.transformCoordsToIndices = transformToMatrix();
  }

  /**
   * Gets the canvas value at the specified point in the plane.
   *
   * @param point a {@link Vector2D} object representing the coordinates of the point in the plane
   * @return 1 if it's a colored pixel, 0 if it's a blank pixel
   */
  public int getPixel(Vector2D point) {
    Vector2D transformedPoint = transformCoordsToIndices.transform(point);
    int x = (int) transformedPoint.getX0();
    int y = (int) transformedPoint.getX1();
    return canvas[x][y];
  }

  /**
   * Sets the canvas value at the specified point in the plane.
   *
   * @param point a {@link Vector2D} object representing the coordinates of the point in the plane
   */
  public void putPixel(Vector2D point) {
    Vector2D transformedPoint = transformCoordsToIndices.transform(point);
    int x = (int) transformedPoint.getX0();
    int y = (int) transformedPoint.getX1();
    if (x >= 0 && x < height && y >= 0 && y < width) {
      canvas[x][y] += 1;
    }
  }

  /**
   * Sets the canvas value at the specified point in the plane, with a specified value corresponding
   * to the color of the pixel (or iteration number).
   *
   * @param point a {@link Vector2D} object representing the coordinates of the point in the plane
   * @param value the value to set the pixel to
   */
  public void putPixelValue(Vector2D point, int value) {
    int x = (int) point.getX1();
    int y = (int) point.getX0();
    if (x >= 0 && x < width && y >= 0 && y < height) {
      canvas[y][x] = value;
    }
  }

  /**
   * Checks if the minimum and maximum coordinates of the fractal are valid.
   *
   * @param min the minimum coordinates of the fractal
   * @param max the maximum coordinates of the fractal
   * @return true if the coordinates are valid, false otherwise
   * @throws IllegalArgumentException if the minimum and maximum coordinates are the same, or if the
   *                                  minimum is greater than the maximum return true if the
   *                                  coordinates are valid, false otherwise
   */
  private boolean checkMinMaxCoords(Vector2D min, Vector2D max) throws IllegalArgumentException {
    if (min.equals(max)) {
      throw new IllegalArgumentException("Min and max coords of the fractal are the same");
    } else if (min.getX0() >= max.getX0() || min.getX1() >= max.getX1()) {
      throw new IllegalArgumentException("Min has to be smaller than max. Min is "
          + min + ", max is " + max);
    }
    return true;
  }

  /**
   * Changes the color of the canvas from light to dark mode, or vice versa.
   */
  public void toggleDarkMode() {
    this.isDarkMode = !isDarkMode;
  }

  /**
   * Sets the color of the canvas to dark mode or light mode.
   *
   * @param isDarkMode true if the canvas should be in dark mode, false if it should be in light
   *                   mode
   */
  public void setDarkMode(boolean isDarkMode) {
    this.isDarkMode = isDarkMode;
  }

  /**
   * Checks if the canvas is in dark mode.
   *
   * @return true if the canvas is in dark mode, false otherwise
   */
  public boolean isDarkMode() {
    return isDarkMode;
  }

  /**
   * Checks if the point is within the canvas.
   *
   * @param point the point to check
   * @param min   the minimum coordinates of the canvas
   * @param max   the maximum coordinates of the canvas
   * @return true if the point is within the canvas, false otherwise
   */
  public boolean checkIfInCanvas(Vector2D point, Vector2D min, Vector2D max) {
    if (point.getX0() < min.getX0() || point.getX1() < min.getX1()) {
      return false;
    } else if (point.getX0() > max.getX0() || point.getX1() > max.getX1()) {
      return false;
    }
    return true;
  }

  /**
   * Gets the height of the canvas.
   *
   * @return the heigth of the canvas
   */
  public int getHeight() {
    return height;
  }

  /**
   * Gets the width of the canvas.
   *
   * @return the width of the canvas
   */
  public int getWidth() {
    return width;
  }


  /**
   * Checks if the canvas has any pixels.
   * <b>Used for testing.</b>
   *
   * @return true if the canvas has pixels, false otherwise
   */
  public boolean hasPixels() {
    for (int[] row : canvas) {
      for (int pixel : row) {
        if (pixel > 0) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Gets the canvas array as a 2D array of int.
   *
   * @return the canvas array
   */
  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * Clears the entire canvas of pixels.
   */
  public void clear() {
    for (int[] row : canvas) {
      Arrays.fill(row, 0);
    }
  }


  /**
   * Transforms the coordinates of the point to the indices of the canvas.
   *
   * @return an {@link AffineTransform2D} object representing the transformed canvas
   */
  private AffineTransform2D transformToMatrix() {
    double a01 = (height - 1) / (minCoords.getX1() - maxCoords.getX1());
    double a10 = (width - 1) / (maxCoords.getX0() - minCoords.getX0());

    Matrix2x2 matrixTransform = new Matrix2x2(0, a01, a10, 0);

    double x0 = ((height - 1) * maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1());
    double x1 = ((width - 1) * minCoords.getX0()) / (minCoords.getX0() - maxCoords.getX0());

    Vector2D vectorTransform = new Vector2D(x0, x1);

    return new AffineTransform2D(matrixTransform, vectorTransform);
  }


}
