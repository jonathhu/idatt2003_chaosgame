package edu.ntnu.stud.model.chaos.fractalgen;

import edu.ntnu.stud.model.chaos.transformations.Transform2D;
import edu.ntnu.stud.model.linalg.Vector2D;

import java.util.List;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The ChaosGameDescription class represents the behavior of a chaos game.
 *
 * @author Jonathan Hübertz
 * @version 0.1
 */
public class ChaosGameDescription {

  private static final Logger LOGGER = LogManager.getLogger(ChaosGameDescription.class);

  private final Vector2D minCoords;
  private final Vector2D maxCoords;

  private final List<Transform2D> transforms;
  private List<Integer> probabilities;

  /**
   * Constructs a {@link ChaosGameDescription} object
   *
   * @param minCoords  the minimum coordinates of the fractal
   * @param maxCoords  the maximum coordinates of the fractal
   * @param transforms the list of transform that are used in the chaos game
   */
  public ChaosGameDescription(Vector2D minCoords, Vector2D maxCoords,
      List<Transform2D> transforms) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
    this.probabilities = null;
  }

  /**
   * Constructs a {@link ChaosGameDescription} object with probabilities
   *
   * @param minCoords     the minimum coordinates of the fractal
   * @param maxCoords     the maximum coordinates of the fractal
   * @param transforms    the list of transform that are used in the chaos game
   * @param probabilities the list of probabilities for each transform
   */
  public ChaosGameDescription(Vector2D minCoords, Vector2D maxCoords,
      List<Transform2D> transforms, List<Integer> probabilities) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
    this.probabilities = probabilities;
  }

  public List<Transform2D> getTransforms() {
    return transforms;
  }

  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  public Vector2D getMinCoords() {
    return minCoords;
  }

  public List<Integer> getProbabilities() {
    return probabilities;
  }


  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    ChaosGameDescription that = (ChaosGameDescription) obj;
    return Objects.equals(minCoords, that.minCoords) &&
        Objects.equals(maxCoords, that.maxCoords) &&
        Objects.equals(transforms, that.transforms);
  }

  @Override
  public int hashCode() {
    return Objects.hash(minCoords, maxCoords, transforms);
  }

  /**
   * Sets the probabilities of the chaos game.
   *
   * @param probabilities
   */
  public void setProbabilities(List<Integer> probabilities) {
    LOGGER.info("Setting new probabilities for the chaos game: {}", () -> probabilities);
    this.probabilities = probabilities;
  }
}


