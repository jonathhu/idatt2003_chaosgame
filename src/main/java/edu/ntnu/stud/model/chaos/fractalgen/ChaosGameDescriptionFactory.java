package edu.ntnu.stud.model.chaos.fractalgen;

import edu.ntnu.stud.model.chaos.transformations.Transform2D;
import edu.ntnu.stud.model.chaos.transformations.AffineTransform2D;
import edu.ntnu.stud.model.linalg.Complex;
import edu.ntnu.stud.model.chaos.transformations.JuliaTransform;
import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Vector2D;
import java.util.ArrayList;
import java.util.List;

import edu.ntnu.stud.utils.TransformationValues;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Factory class for creating different chaos game descriptions.
 * <p>
 * This class provides static methods for creating different chaos game descriptions. The
 * descriptions are created with the necessary transforms and min and max points.
 * {@link ChaosGameDescription} instances are returned.
 * </p>
 *
 * @author Jonathan Hübertz
 * @version 1.0
 * @see ChaosGameDescription
 */

public class ChaosGameDescriptionFactory {

  static List<Transform2D> transforms;

  private ChaosGameDescriptionFactory() {
    throw new IllegalStateException("ChaosGameDescriptionFactory is a utility class");
  }

  /**
   * A static method that creates a standard chaos game description. with predefined transforms and
   * min and max points.
   *
   * @return a {@link ChaosGameDescription} instance
   */
  public static ChaosGameDescription createDescriptionProbability(Vector2D min, Vector2D max,
      List<Transform2D> transforms,
      List<Integer> probabilities) {
    return new ChaosGameDescription(min, max, transforms, probabilities);
  }

  /**
   * A static method that creates a Sierpinski chaos game description. with predefined transforms
   * and min and max points.
   *
   * @return a {@link ChaosGameDescription} instance
   */
  public static ChaosGameDescription createSierpinskiTriangle() {
    transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5),
        new Vector2D(0, 0)));
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5),
        new Vector2D(0.25, 0.5)));
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5),
        new Vector2D(0.5, 0)));
    Vector2D min = new Vector2D(0, 0);
    Vector2D max = new Vector2D(1, 1);
    return new ChaosGameDescription(min, max, transforms);
  }

  /**
   * A static method that creates a Barnsley chaos game description. with predefined transforms and
   * min and max points.
   *
   * @return a {@link ChaosGameDescription} instance
   */
  public static ChaosGameDescription createBarnsleyFern() {
    transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0, 0, 0, 0.16),
        new Vector2D(0, 0)));
    transforms.add(
        new AffineTransform2D(new Matrix2x2(0.85, 0.04, -0.04, 0.85),
            new Vector2D(0, 1.6)));
    transforms.add(
        new AffineTransform2D(new Matrix2x2(0.2, -0.26, 0.23, 0.22),
            new Vector2D(0, 1.6)));
    transforms.add(
        new AffineTransform2D(new Matrix2x2(-0.15, 0.28, 0.26, 0.24),
            new Vector2D(0, 0.44)));
    Vector2D min = new Vector2D(-2.65, 0);
    Vector2D max = new Vector2D(2.65, 10);
    return new ChaosGameDescription(min, max, transforms, List.of(2, 84, 7, 7));
  }

  /**
   * A static method that creates a Levy chaos game description. with predefined transforms and min
   * and max points.
   *
   * @return a {@link ChaosGameDescription} instance
   */
  public static ChaosGameDescription createLevyCurveAffine() {
    transforms = new ArrayList<>();
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, -0.5, 0.5, 0.5),
        new Vector2D(0, 0)));
    transforms.add(new AffineTransform2D(new Matrix2x2(0.5, 0.5, -0.5, 0.5),
        new Vector2D(0.5, 0)));
    Vector2D min = new Vector2D(0, 0);
    Vector2D max = new Vector2D(2, 2);
    return new ChaosGameDescription(min, max, transforms);
  }

  /**
   * A static method that creates a Julia chaos game description. with predefined transforms and min
   * and max points.
   *
   * @return a {@link ChaosGameDescription} instance
   */
  public static ChaosGameDescription createPreDefinedJuliaDescriptionStandard() {
    return createPreDefinedJuliaDescriptionStandard(new Complex(-0.76,
        0.0745));
  }

  /**
   * A static method that creates a Julia chaos game description that looks like MandelBrot with
   * predefined transforms and min and max points.
   *
   * @return a {@link ChaosGameDescription} instance
   */

  public static ChaosGameDescription createPreDefinedJuliaDescriptionLikeMandel() {
    transforms = new ArrayList<>();
    Transform2D julia1 = new JuliaTransform(new Complex(-0.8, 0), 1);
    transforms.add(julia1);
    Vector2D min = new Vector2D(-1.5, -1.5);
    Vector2D max = new Vector2D(1.5, 1.5);
    return new ChaosGameDescription(min, max, transforms);
  }

  /**
   * A static method that creates a Julia chaos game description. with predefined transforms and min
   * and max points.
   *
   * @param c the complex number that defines the Julia set
   * @return a {@link ChaosGameDescription} instance
   */
  public static ChaosGameDescription createPreDefinedJuliaDescriptionStandard(Complex c) {
    transforms = new ArrayList<>();

    Transform2D julia1 = new JuliaTransform(c, 1);
    Transform2D julia2 = new JuliaTransform(c, -1);

    Vector2D min = new Vector2D(-1.5, -1.5);
    Vector2D max = new Vector2D(1.5, 1.5);

    transforms.add(julia1);
    transforms.add(julia2);

    return new ChaosGameDescription(min, max, transforms);
  }


  /**
   * A static method that creates a customJulia chaos game with the julia square root
   * transformation.
   *
   * @param c    the complex number that defines the Julia set
   * @param minX the minimum x coordinate of the fractal
   * @param maxX the maximum x coordinate of the fractal
   * @param minY the minimum y coordinate of the fractal
   * @param maxY the maximum y coordinate of the fractal
   * @return a {@link ChaosGameDescription} instance
   */
  public static ChaosGameDescription createCustomJuliaSquareRoot(Complex c, double minX,
      double maxX,
      double minY, double maxY) {
    transforms = new ArrayList<>();
    Transform2D julia1 = new JuliaTransform(c, 1);
    Transform2D julia2 = new JuliaTransform(c, -1);

    Vector2D min = new Vector2D(minX, minY);
    Vector2D max = new Vector2D(maxX, maxY);

    transforms.add(julia1);
    transforms.add(julia2);

    return new ChaosGameDescription(min, max, transforms);
  }

  /**
   * A static method that creates a customJulia set.
   *
   * @param c    the complex number that defines the Julia set
   * @param minX the minimum x coordinate of the fractal
   * @param maxX the maximum x coordinate of the fractal
   * @param minY the minimum y coordinate of the fractal
   * @param maxY the maximum y coordinate of the fractal
   * @return a {@link ChaosGameDescription} instance
   */
  public static ChaosGameDescription createCustomJuliaSet(Complex c, double minX, double maxX,
      double minY, double maxY) {
    transforms = new ArrayList<>();
    Transform2D julia1 = new JuliaTransform(c, 1);
    Vector2D min = new Vector2D(minX, minY);
    Vector2D max = new Vector2D(maxX, maxY);
    transforms.add(julia1);

    return new ChaosGameDescription(min, max, transforms);
  }


  /**
   * A static method that creates a custom affine set.
   *
   * @param transformations the list of transformations
   * @param minX            the minimum x coordinate of the fractal
   * @param maxX            the maximum x coordinate of the fractal
   * @param minY            the minimum y coordinate of the fractal
   * @param maxY            the maximum y coordinate of the fractal
   * @param probabilities   the list of probabilities. Can be null
   * @return a {@link ChaosGameDescription} instance
   */
  public static ChaosGameDescription createCustomAffineSet(List<TransformationValues>
      transformations,
      double minX, double maxX, double minY,
      double maxY,
      List<Integer> probabilities) {
    transforms = new ArrayList<>();
    for (TransformationValues values : transformations) {
      transforms.add(new AffineTransform2D(new Matrix2x2(values.getMatrixValues()[0][0],
          values.getMatrixValues()[0][1], values.getMatrixValues()[1][0],
          values.getMatrixValues()[1][1]),
          new Vector2D(values.getVectorValues()[0], values.getVectorValues()[1])));
    }

    if (probabilities == null) {
      return new ChaosGameDescription(new Vector2D(minX, minY), new Vector2D(maxX, maxY),
          transforms);
    }
    return new ChaosGameDescription(new Vector2D(minX, minY), new Vector2D(maxX, maxY),
        transforms, probabilities);
  }

  /**
   * A static method that creates an empty description.
   *
   * @return a {@link ChaosGameDescription} instance
   */
  public static ChaosGameDescription createEmptyDescription() {
    return new ChaosGameDescription(new Vector2D(0, 0), new Vector2D(1, 1), new ArrayList<>());
  }

}
