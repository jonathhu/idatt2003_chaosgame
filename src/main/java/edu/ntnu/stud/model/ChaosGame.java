package edu.ntnu.stud.model;

import edu.ntnu.stud.model.chaos.fractalgen.ChaosCanvas;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescription;
import edu.ntnu.stud.model.chaos.transformations.JuliaTransform;
import edu.ntnu.stud.model.chaos.transformations.Transform2D;
import edu.ntnu.stud.model.linalg.Complex;
import edu.ntnu.stud.model.linalg.Vector2D;

import edu.ntnu.stud.observer.ChaosGameSubject;
import java.util.List;
import java.util.Random;

/**
 * The ChaosGame class represents a chaos game.
 *
 * <p>
 * This class connects the chaos game description with the canvas and runs the chaos game.
 * </p>
 *
 * @author Aryan Malekian
 * @version 0.1
 */
public class ChaosGame extends ChaosGameSubject {

  private ChaosCanvas canvas;
  private int canvasWidth;
  private int canvasHeight;
  private ChaosGameDescription description;
  private Vector2D currentPoint;
  private final Random random = new Random();
  private int steps;
  private int maxIterations;
  private int escapeRadius;

  /**
   * Constructs a new {@code ChaosGame} with the specified description, width, and height. Seed
   * allows for the random number generator to be seeded, for debugging purposes.
   *
   * @param description the description of the chaos game
   * @param width       the width of the canvas
   * @param height      the height of the canvas
   */
  public ChaosGame(ChaosGameDescription description, int width, int height) {
    this.description = description;
    this.canvas = new ChaosCanvas(width, height, description.getMinCoords(),
        description.getMaxCoords());
    this.currentPoint = new Vector2D(0, 0);
    this.steps = 50000;
    this.maxIterations = 124;
    this.escapeRadius = 4;
    this.canvasWidth = width;
    this.canvasHeight = height;
  }


  /**
   * Runs the chaos game for the specified number of steps. The chaos game will use the transforms
   * specified in the description to transform the current point and put a pixel on the canvas.
   */
  public void runSteps() {

    if (description.getProbabilities() != null) {
      runStepsWithProbabilities(description.getProbabilities());
    } else {
      runStepsUniFormally();
    }
  }

  /**
   * Runs the chaos game for the specified number of steps uniformally distributed. The chaos game
   * will use the transforms specified in the description to transform the current point and put a
   * pixel on the canvas.
   *
   */
  private void runStepsUniFormally() {
    canvas.clear();

    List<Transform2D> transforms = description.getTransforms();
    int numTransforms = transforms.size();

    for (int i = 0; i < steps; i++) {
      int transformIndex = random.nextInt(numTransforms);
      currentPoint = transforms.get(transformIndex).transform(currentPoint);

      if (canvas.checkIfInCanvas(currentPoint, description.getMinCoords(),
          description.getMaxCoords())) {
        canvas.putPixel(currentPoint);
      }
    }
  }

  /**
   * Runs the Julia set fractal for the specified number of iterations. The Julia set fractal will
   * use the transforms specified in the description to transform the current point and put a pixel
   * on the canvas.
   */
  public void runIterations() {
    canvas.clear();

    Complex c = (new Complex(0, 0));
    for (Transform2D transform : description.getTransforms()) {
      if (transform instanceof JuliaTransform juliaTransform && juliaTransform.getSign() == 1) {
        c = juliaTransform.getConstantPoint();
        break;
      }
    }

    double cx = c.getRealPart();
    double cy = c.getImaginaryPart();

    for (int x0 = 0; x0 < canvasWidth; x0++) {
      for (int x1 = 0; x1 < canvasHeight; x1++) {
        evaluatePoint(x0, x1, cx, cy);
      }
    }

  }


  /**
   * Evaluates a point in the Julia set fractal, to determine what the property of the point should
   * be. The point is evaluated by iterating over the point and checking if it escapes the radius
   * of the Julia set. The color value of the point is then computed based on the number of
   * iterations it took to escape the radius, by the computeColorValue method.
   *
   * @param x0 the x-coordinate of the point
   * @param x1 the y-coordinate of the point
   * @param cx the x-coordinate of the constant point
   * @param cy the y-coordinate of the constant point
   */
  public void evaluatePoint(int x0, int x1, double cx, double cy) {
    double[] coords = scaleCoords(x0, x1);
    double zx = coords[0];
    double zy = coords[1];

    int iteration = 0;
    while (zx * zx + zy * zy < escapeRadius && iteration < this.maxIterations) {
      double tmp = zx * zx - zy * zy;
      zy = 2.0 * zx * zy + cy;
      zx = tmp + cx;
      iteration++;
    }

    Vector2D point = new Vector2D(x1, x0);
    canvas.putPixelValue(point, iteration);
  }


  /**
   * Scales the coordinates of the point to the range [-2, 2], as the Julia set fractal is
   * typically defined in this range.
   *
   * @param x0 is the x-coordinate of the point/pixel
   * @param x1 is the y-coordinate of the point/pixel
   *
   * @return the scaled coordinates
   */
  private double[] scaleCoords(double x0, double x1) {
    double[] coords = new double[2];
    coords[0] = (x0 - (double) canvasWidth / 2) * 4.0 / canvasWidth;
    coords[1] = (x1 - (double) canvasHeight / 2) * 4.0 / canvasWidth;
    return coords;
  }


  /**
   * Runs the chaos game for the specified number of steps with the specified probabilities. The
   * chaos game will use the transforms specified in the description to transform the current point
   * and put a pixel on the canvas.
   * @param probabilities the probabilities of each transform occurring
   */
  private void runStepsWithProbabilities(List<Integer> probabilities) {
    canvas.clear();

    List<Transform2D> transforms = description.getTransforms();
    int numTransforms = transforms.size();

    for (int i = 0; i < steps; i++) {
      int test = random.nextInt(100);
      int transformIndex = -1;
      for (int j = 0; j < probabilities.size(); j++) {
        test -= probabilities.get(j);
        if (test < 0) {
          transformIndex = j;
          break;
        }
      }
      if (transformIndex == -1) {
        transformIndex = numTransforms - 1;
      }
      currentPoint = transforms.get(transformIndex).transform(currentPoint);
      canvas.putPixel(currentPoint);
    }

  }

  /**
   * Resets the current point to the origin (0, 0).
   */
  public void resetCurrentPoint() {
    this.currentPoint = new Vector2D(0, 0);
  }

  public int getSteps() {
    return steps;
  }

  public Vector2D getCurrentPoint() {
    return currentPoint;
  }


  public ChaosCanvas getCanvas() {
    return canvas;
  }

  public ChaosGameDescription getDescription() {
    return description;
  }

  public int getMaxIterations() {
    return maxIterations;
  }

  public int getEscapeRadius() {
    return escapeRadius;
  }

  /**
   * Sets the description of the chaos game.
   *
   * @param newDescription the new description
   */
  public void setDescription(ChaosGameDescription newDescription) {
    boolean oldIsDarkMode = canvas.isDarkMode();
    this.description = newDescription;
    this.canvas = new ChaosCanvas(canvas.getWidth(), canvas.getHeight(),
        this.description.getMinCoords(), this.description.getMaxCoords());
    this.canvas.setDarkMode(oldIsDarkMode);
    updateCanvasObservers();
  }

  /**
   * Sets the description of the chaos game with probabilities.
   * @param newDescription the new description
   * @param probabilities the probabilities of each transform occurring
   */
  public void setDescription(ChaosGameDescription newDescription, List<Integer> probabilities) {
    boolean oldIsDarkMode = canvas.isDarkMode();
    this.description = newDescription;
    this.description.setProbabilities(probabilities);
    this.canvas = new ChaosCanvas(canvas.getWidth(), canvas.getHeight(),
        this.description.getMinCoords(), this.description.getMaxCoords());
    this.canvas.setDarkMode(oldIsDarkMode);
    updateCanvasObservers();
  }

  /**
   * Sets the description of the Julia set fractal.
   *
   * @param newDescription the new description
   */
  public void setJuliaSetDescription(ChaosGameDescription newDescription) {
    boolean oldIsDarkMode = canvas.isDarkMode();
    this.description = newDescription;
    this.canvas = new ChaosCanvas(canvas.getWidth(), canvas.getHeight(),
        this.description.getMinCoords(), this.description.getMaxCoords());
    this.canvas.setDarkMode(oldIsDarkMode);
    updateCanvasToJuliaObservers();
  }


  /**
   * Sets the amount of steps the chaos game should run and notifies the observers.
   *
   * @param amount the amount of steps
   */
  public void setStepsAmount(int amount) {
    if (amount >= 0) {
      steps = amount;
    } else {
      steps = 0;
    }
    updateCanvasObservers();
  }

  /**
   * Sets the maximum amount of iterations the Julia set fractal should run and
   * notifies the observers.
   * @param maxIterations the maximum amount of iterations
   */
  public void setMaxIterations(int maxIterations) {
    if (maxIterations >= 0) {
      this.maxIterations = maxIterations;
    }
    updateCanvasToJuliaObservers();
  }

  /**
   * Sets the escape radius of the Julia set fractal and notifies the observers.
   * @param escapeRadius the escape radius
   */
  public void setEscapeRadius(int escapeRadius) {
    if (escapeRadius >= 0) {
      this.escapeRadius = escapeRadius;
    }
    updateCanvasToJuliaObservers();
  }

  /**
   * Toggles the dark mode of the canvas and notifies the observers.
   */
  public void toggleDarkMode() {
    canvas.toggleDarkMode();
    if (description.getTransforms().isEmpty()) {
      updateThemeObservers();
    } else if (description.getTransforms().size() == 1) {
      updateCanvasToJuliaObservers();
    } else {
      updateCanvasObservers();
    }
  }
}
