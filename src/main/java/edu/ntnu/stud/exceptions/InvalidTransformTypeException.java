package edu.ntnu.stud.exceptions;


/**
 * Unchecked exception thrown when an invalid transform type is given.
 */
public class InvalidTransformTypeException extends IllegalArgumentException {
  public InvalidTransformTypeException(String message) {
    super("Invalid transform type: " + message + ". Expected Affine2D or Julia.");
  }
}
