package edu.ntnu.stud.exceptions;

import java.util.NoSuchElementException;

/**
 * Unchecked exception thrown when a file is incorrectly formatted.
 */
public class IncorrectFileFormatException extends NoSuchElementException {
  public IncorrectFileFormatException(String path) {
    super("File is incorrectly formatted. \nExpected: src/main/resources/<file> \nActual: " + path);
  }
}
