package edu.ntnu.stud.exceptions;

import java.io.FileNotFoundException;

/**
 * Checked exception thrown when a file is not found.
 */
public class FileNotFoundCustomException extends FileNotFoundException {

  public FileNotFoundCustomException(String path) {
    super("No file not found in path: " + path);
  }
}
