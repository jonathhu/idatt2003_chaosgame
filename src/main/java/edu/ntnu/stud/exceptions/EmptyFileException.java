package edu.ntnu.stud.exceptions;

/**
 * Unchecked exception thrown when a file is empty.
 */
public class EmptyFileException extends RuntimeException{

  public EmptyFileException(String path) {
    super("File is empty: " + path);
  }
}
