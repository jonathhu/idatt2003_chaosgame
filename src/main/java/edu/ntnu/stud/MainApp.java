package edu.ntnu.stud;

import edu.ntnu.stud.model.filehandling.ChaosGameFileHandlerStrategy;
import edu.ntnu.stud.model.filehandling.TxtChaosGameFileHandler;
import edu.ntnu.stud.view.LaunchView;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The main class of the application.
 */
public class MainApp {

  /**
   * The main method of the application.
   *
   * @param args the arguments
   */
  public static void main(String[] args) {
    ChaosGameFileHandlerStrategy strategy = new TxtChaosGameFileHandler();
    LaunchView launchView = new LaunchView(strategy);
    launchView.launchApp(args);
  }
}