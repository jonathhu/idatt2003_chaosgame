package edu.ntnu.stud.ui;

import edu.ntnu.stud.exceptions.FileNotFoundCustomException;
import edu.ntnu.stud.model.filehandling.ChaosGameFileHandlerStrategy;
import edu.ntnu.stud.model.filehandling.TxtChaosGameFileHandler;
import edu.ntnu.stud.model.ChaosGame;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescription;
import edu.ntnu.stud.model.filehandling.ChaosGameFileHandler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class CommandLineInterface {

  private final HashMap<Integer, Runnable> menu = new HashMap<>();
  private final Scanner scanner = new Scanner(System.in);
  private final ChaosGameFileHandlerStrategy strategy = new TxtChaosGameFileHandler();

  ChaosGame game;
  ChaosGameDescription description;
  ChaosGameFileHandler fileHandler = new ChaosGameFileHandler(strategy);

  public void init() {
    menu.put(1, this::readDescriptionFromFile);
    menu.put(2, this::writeDescriptionToFile);
    menu.put(3, this::runSteps);
    menu.put(4, this::showCanvas);
    menu.put(5, () -> System.exit(0));
  }

  public void showMenu() {
    System.out.println("1. Read description from file");
    System.out.println("2. Write description to file");
    System.out.println("3. Run steps");
    System.out.println("4. Show canvas");
    System.out.println("5. Exit");
  }

  public void start() {
    while (true) {
      showMenu();
      System.out.print("Choose an option: ");
      int choice = Integer.parseInt(scanner.nextLine());
      if (choice > 0 && choice <= menu.size()) {
        menu.get(choice).run();
      } else if (choice == 5) {
        break;
      } else {
        System.out.println("Invalid option.");
      }
    }
  }

  public static void main(String[] args) {
    CommandLineInterface cli = new CommandLineInterface();
    cli.init();
    cli.start();
  }

  private void readDescriptionFromFile() {
    ChaosGameDescription newDescription;
    System.out.print("Enter the file path: ");
    String fileName = scanner.nextLine();
    try {
      newDescription = fileHandler.readFromFile(fileName);
    } catch (FileNotFoundCustomException e) {
      throw new IllegalArgumentException(e);
    }
    this.description = newDescription;
    game = new ChaosGame(description, 100, 100);
  }

  private void writeDescriptionToFile() {
    System.out.print("Enter the file name: ");
    String fileName = scanner.nextLine();
    try {
      fileHandler.writeToFile(description, fileName);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void runSteps() {
    System.out.print("Enter the number of steps: ");
    int steps = Integer.parseInt(scanner.nextLine());
    game.runSteps();
  }

  private void showCanvas() {
    int[][] canvas = game.getCanvas().getCanvasArray();
    for (int[] row : canvas) {
      for (int pixel : row) {
        System.out.print(pixel == 1 ? "*" : " ");
      }
      System.out.println();
    }
  }


}
