package edu.ntnu.stud.controller;

import edu.ntnu.stud.model.chaos.transformations.Transform2D;
import edu.ntnu.stud.model.chaos.transformations.AffineTransform2D;
import edu.ntnu.stud.model.chaos.transformations.JuliaTransform;
import edu.ntnu.stud.model.filehandling.ChaosGameFileHandlerStrategy;
import edu.ntnu.stud.model.ChaosGame;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescription;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescriptionFactory;
import edu.ntnu.stud.model.filehandling.ChaosGameFileHandler;
import edu.ntnu.stud.model.linalg.Vector2D;
import edu.ntnu.stud.observer.ChaosGameObserver;
import edu.ntnu.stud.utils.ChaosGameValidations;
import edu.ntnu.stud.view.components.FractalCanvas;
import edu.ntnu.stud.utils.TransformationValues;
import javafx.beans.property.BooleanProperty;
import javafx.scene.control.TextField;
import javafx.scene.input.ScrollEvent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The ChaosGameController class is responsible for handling the communication between the model and
 * the view.
 * <p> The controller is responsible for handling the user input and updating the model
 * accordingly.</p>
 *
 * @author Jonathan Hübertz, Aryana Malekian
 **/

public class ChaosGameController {

  private static final Logger LOGGER = LogManager.getLogger(ChaosGameController.class);

  private final ChaosGame chaosGame;
  private final FractalCanvas fractalCanvas;
  private final ChaosGameFileHandler fileHandler;
  private final ChaosGameValidations validations;
  private double zoomLevel = 1.0;

  /**
   * Constructor for the ChaosGameController class.
   *
   * @param width    the width of the canvas
   * @param height   the height of the canvas
   * @param strategy the file handler strategy to use
   */

  public ChaosGameController(int width, int height, ChaosGameFileHandlerStrategy strategy) {
    chaosGame = new ChaosGame(ChaosGameDescriptionFactory.createEmptyDescription(), width, height);
    fractalCanvas = new FractalCanvas(chaosGame);
    fileHandler = new ChaosGameFileHandler(strategy);
    validations = new ChaosGameValidations();
  }

  /**
   * Sets a {@link ChaosGameDescription} of the {@link ChaosGame}.
   *
   * @param description the {@link ChaosGameDescription} to set
   *
   *                    <p> If the description does not contain probabilities, the description will
   *                    instantiate with the constructor that does not take probabilities as an
   *                    argument.</p>
   *
   *                    <p> If the description does not contain probabilities, the description will
   *                    instantiate with the constructor that does not take probabilities as an
   *                    argument.</p>
   */

  public synchronized void setGameDescription(ChaosGameDescription description) {
    if (description.getProbabilities() == null) {
      LOGGER.log(Level.INFO, "Setting game description without probabilities");
      chaosGame.setDescription(description);
    } else {
      LOGGER.log(Level.INFO, "Setting game description with probabilities");
      chaosGame.setDescription(description, description.getProbabilities());

    }
  }

  /**
   * Sets a {@link ChaosGameDescription} of the {@link ChaosGame} to a Julia set.
   *
   * @param description the Julia set {@link ChaosGameDescription} to set
   */

  public synchronized void setJuliaDescription(ChaosGameDescription description) {
    LOGGER.log(Level.INFO, "Setting Julia description");
    chaosGame.setJuliaSetDescription(description);
  }


  /**
   * Check if the current description is a Julia set.
   *
   * @return true if the current description is a Julia set, false otherwise
   */
  public boolean isCurrentDescriptionJuliaSet() {
    long count = chaosGame.getDescription().getTransforms().stream()
        .filter(JuliaTransform.class::isInstance)
        .count();
    return count == 1;
  }

  /**
   * Creates a {@link ChaosGameDescription} with probabilities.
   *
   * @param min           the minimum coordinates
   * @param max           the maximum coordinates
   * @param transforms    the list of transformations
   * @param probabilities the list of probabilities
   * @return a {@link ChaosGameDescription} with probabilities
   */
  public ChaosGameDescription createChaosDescriptionProbability(Vector2D min, Vector2D max,
      List<Transform2D> transforms,
      List<Integer> probabilities) {
    return ChaosGameDescriptionFactory.createDescriptionProbability(min, max, transforms,
        probabilities);
  }


  /**
   * Handles scroll event to update zoom level accordingly.
   *
   * @param scrollEvent the scroll event
   * @param width       the width of the canvas
   * @param height      the height of the canvas
   * @param layoutX     the x-coordinate of the layout
   * @param layoutY     the y-coordinate of the layout
   */
  public void handleScrollToUpdateZoom(ScrollEvent scrollEvent, double width, double height,
      double layoutX, double layoutY) {
    double zoomFactor = calculateZoomFactor(scrollEvent.getDeltaY());
    zoomLevel *= zoomFactor;

    double mouseX = (scrollEvent.getX() - layoutX) / width;
    double mouseY = (scrollEvent.getY() - layoutY) / height;

    double minCoordsX = chaosGame.getDescription().getMinCoords().getX0();
    double minCoordsY = chaosGame.getDescription().getMinCoords().getX1();
    double maxCoordsX = chaosGame.getDescription().getMaxCoords().getX0();
    double maxCoordsY = chaosGame.getDescription().getMaxCoords().getX1();

    if (mouseX >= minCoordsX && mouseX <= maxCoordsX && mouseY >= minCoordsY
        && mouseY <= maxCoordsY) {
      updateFractalZoom(zoomFactor, mouseX, mouseY);
    } else {
      double centerX = (minCoordsX + maxCoordsX) / 2;
      double centerY = (minCoordsY + maxCoordsY) / 2;
      updateFractalZoom(zoomFactor, centerX, centerY);
    }
  }


  /**
   * Updates {@link ChaosGameDescription} with new values based after zooming.
   *
   * <p>
   * The new values are calculated based on the zoom factor, the x-coordinate of the mouse, and the
   * y-coordinate of the mouse.
   * </p>
   *
   * @param zoomFactor the zoom factor
   * @param mouseX     the x-coordinate of the mouse
   * @param mouseY     the y-coordinate of the mouse
   */
  private void updateFractalZoom(double zoomFactor, double mouseX, double mouseY) {
    double oldMinX = chaosGame.getDescription().getMinCoords().getX0();
    double oldMinY = chaosGame.getDescription().getMinCoords().getX1();
    double oldMaxX = chaosGame.getDescription().getMaxCoords().getX0();
    double oldMaxY = chaosGame.getDescription().getMaxCoords().getX1();

    double newMinX = oldMinX + (mouseX - oldMinX) * (1 - zoomFactor);
    double newMinY = oldMinY + (mouseY - oldMinY) * (1 - zoomFactor);
    double newMaxX = oldMaxX + (mouseX - oldMaxX) * (1 - zoomFactor);
    double newMaxY = oldMaxY + (mouseY - oldMaxY) * (1 - zoomFactor);

    if (isCurrentDescriptionJuliaSet()) {
      setJuliaDescription(createChaosDescriptionProbability(new Vector2D(newMinX, newMinY),
          new Vector2D(newMaxX, newMaxY), chaosGame.getDescription().getTransforms(),
          chaosGame.getDescription().getProbabilities()));
      LOGGER.log(Level.INFO, "Updated Julia set zoom");

    } else {
      setGameDescription(createChaosDescriptionProbability(new Vector2D(newMinX, newMinY),
          new Vector2D(newMaxX, newMaxY), chaosGame.getDescription().getTransforms(),
          chaosGame.getDescription().getProbabilities()));
      LOGGER.log(Level.INFO, "Updated normal game zoom");
    }
  }

  /**
   * Calculates the zoom factor based on the deltaY value.
   *
   * @param deltaY the deltaY value
   * @return the zoom factor
   */
  private double calculateZoomFactor(double deltaY) {
    final double zoomIntensity = 0.005;

    if (deltaY > 0) {
      LOGGER.debug("Zooming out");
      return 1 - zoomIntensity;
    } else {
      LOGGER.debug("Zooming in");
      return 1 + zoomIntensity;
    }

  }

  /**
   * Gets the current zoom level.
   *
   * @return the current zoom level
   */
  public double getZoomLevel() {
    return zoomLevel;
  }

  /**
   * Updates {@link ChaosGameDescription} with new values after dragging.
   *
   * <p>
   * The new values are calculated based on the change in x-coordinate, the change in y-coordinate,
   * the width of the canvas, and the height of the canvas.
   * </p>
   *
   * @param deltaX the change in x-coordinate
   * @param deltaY the change in y-coordinate
   * @param width  the width of the canvas
   * @param height the height of the canvas
   */
  public void handleDragToUpdateZoom(double deltaX, double deltaY, double width, double height) {

    double oldMinX = chaosGame.getDescription().getMinCoords().getX0();
    double oldMinY = chaosGame.getDescription().getMinCoords().getX1();
    double oldMaxX = chaosGame.getDescription().getMaxCoords().getX0();
    double oldMaxY = chaosGame.getDescription().getMaxCoords().getX1();

    double newMinX = oldMinX - deltaX / width;
    double newMinY = oldMinY + deltaY / height;
    double newMaxX = oldMaxX - deltaX / width;
    double newMaxY = oldMaxY + deltaY / height;

    if (isCurrentDescriptionJuliaSet()) {
      setJuliaDescription(createChaosDescriptionProbability(new Vector2D(newMinX, newMinY),
          new Vector2D(newMaxX, newMaxY), chaosGame.getDescription().getTransforms(),
          chaosGame.getDescription().getProbabilities()));
      LOGGER.debug("Handled Julia dragevent");
    } else {
      setGameDescription(createChaosDescriptionProbability(new Vector2D(newMinX, newMinY),
          new Vector2D(newMaxX, newMaxY), chaosGame.getDescription().getTransforms(),
          chaosGame.getDescription().getProbabilities()));
      LOGGER.debug("Handled normal game dragevent");
    }
  }


  /**
   * @return the {@link ChaosGameDescription} of the {@link ChaosGame}
   */
  public ChaosGameDescription getFractalDescription() {
    return chaosGame.getDescription();
  }

  /**
   * Sets new amount of steps for the {@link ChaosGame}.
   *
   * @param steps the new amount of steps
   */
  public synchronized void runSteps(int steps) {
    LOGGER.info("run steps set to: {}", () -> steps);
    chaosGame.setStepsAmount(steps);
  }

  /**
   * Sets new amount of iterations for Julia set {@link ChaosGame}.
   *
   * @param iterations the number of iterations to run for the Julia set
   */

  public synchronized void runIterations(int iterations) {
    LOGGER.info("run iterations set to: {}", () -> iterations);
    chaosGame.setMaxIterations(iterations);
  }

  /**
   * Gets the current amount of steps the {@link ChaosGame} is set to run.
   *
   * @return the current amount of steps
   */
  public int getSteps() {
    return chaosGame.getSteps();
  }

  /**
   * Adds an observer to the {@link ChaosGame} subject.
   *
   * @param observer the observer to add
   */
  public void addObserver(ChaosGameObserver observer) {
    LOGGER.info("Adding observer: {}", () -> observer);
    chaosGame.addObserver(observer);
  }

  /**
   * Removes an observer from the {@link ChaosGame} subject.
   *
   * @param observer the observer to remove
   */
  public void removeObserver(ChaosGameObserver observer) {
    LOGGER.info("Adding observer: {}", () -> observer);
    chaosGame.removeObserver(observer);
  }

  /**
   * Gets the {@link ChaosGame}.
   *
   * @return the {@link ChaosGame}
   */
  public ChaosGame getChaosGame() {
    return chaosGame;
  }


  /**
   * Gets the {@link FractalCanvas}.
   *
   * @return the {@link FractalCanvas}
   */

  public FractalCanvas getFractalCanvas() {
    return fractalCanvas;
  }

  /**
   * Gets the {@link ChaosGameFileHandler}.
   *
   * @return the {@link ChaosGameFileHandler}
   */

  public String getTransformType() {
    Transform2D transform = chaosGame.getDescription().getTransforms().get(0);
    if (transform instanceof AffineTransform2D) {
      return "affine";
    } else if (transform instanceof JuliaTransform) {
      return "julia";
    } else {
      return "unknown";
    }
  }

  /**
   * Converts the affine description to view data.
   *
   * <p>
   * The affine description is converted to view data by extracting the matrix values and vector
   * values from the affine transforms. The matrix values are stored in a 2D array and the vector
   * values are stored in a 1D array.
   * </p>
   *
   * @return a list of {@link TransformationValues} each containing the affine description data
   */

  public List<TransformationValues> convertAffineDescriptionToViewData() {
    List<Transform2D> transforms = chaosGame.getDescription().getTransforms();
    List<TransformationValues> transformationValuesList = new ArrayList<>();

    for (Transform2D transform : transforms) {
      if (transform instanceof AffineTransform2D) {
        AffineTransform2D affine = (AffineTransform2D) transform;
        double[][] matrixValues = new double[][]{
            {affine.getMatrix().getA00(), affine.getMatrix().getA01()},
            {affine.getMatrix().getA10(), affine.getMatrix().getA11()}
        };
        double[] vectorValues = new double[]{affine.getVector().getX0(),
            affine.getVector().getX1()};
        transformationValuesList.add(new TransformationValues(matrixValues, vectorValues));
      }
    }
    return transformationValuesList;
  }

  /**
   * Converts the Julia description to view data.
   *
   * <p>
   * The Julia description is converted to view data by extracting the constant values from the
   * Julia transforms. The constant values are stored in a 1D array.
   * </p>
   *
   * @return the {@link TransformationValues} containing the Julia description data
   */
  public TransformationValues convertJuliaDescriptionToViewData() {
    List<Transform2D> transforms = chaosGame.getDescription().getTransforms();
    TransformationValues transformationValues = null;

    for (Transform2D transform : transforms) {
      if (transform instanceof JuliaTransform) {
        JuliaTransform julia = (JuliaTransform) transform;
        double[] constantValues = new double[]{julia.getConstantPoint().getRealPart(),
            julia.getConstantPoint().getImaginaryPart()};
        transformationValues = new TransformationValues(constantValues);
        break;
      }
    }
    return transformationValues;
  }

  /**
   * Resets the ChaosGame's current point to the origin (0, 0).
   */
  public synchronized void resetCurrentPoint() {
    chaosGame.resetCurrentPoint();

  }

  /**
   * Validates that the input in a {@link javafx.scene.control.TextField} is a double value.
   *
   * @param textField the {@link javafx.scene.control.TextField} to validate
   * @return an error message if the input is invalid, an empty string otherwise
   */
  public String validateOnlyDoubleInput(TextField textField) {
    BooleanProperty isValid = validations.validateOnlyDoubleInput(textField);
    return isValid.get() ? ""
        : "Invalid input entered into a textfield where only double values are permitted";
  }

  /**
   * Validates that the min coordinates are smaller than the max coordinates.
   *
   * <p>
   * Validates that maxX is greater than minX and maxY is greater than minY.
   * </p>
   *
   * @param minXField the TextField for the min x-coordinate
   * @param maxXField the TextField for the max x-coordinate
   * @param minYField the TextField for the min y-coordinate
   * @param maxYField the TextField for the max y-coordinate
   * @return an error message if the min coordinates are greater than the max coordinates, an empty
   */
  public String validateMinMaxCoords(TextField minXField, TextField maxXField, TextField minYField,
      TextField maxYField) {
    BooleanProperty isValid = validations.validateMinMaxCoords(maxXField, minXField, minYField,
        maxYField);

    if (validateOnlyDoubleInput(minXField).equals("") && validateOnlyDoubleInput(maxXField).equals(
        "") && validateOnlyDoubleInput(minYField).equals("") && validateOnlyDoubleInput(
        maxYField).equals("")) {
      return isValid.get() ? ""
          : "Min has to be smaller than max. \nMin is: "
              + "(" + minXField.getText().toString() + ", " + minYField.getText().toString() + ")"
              + "\nMax is " + "(" + maxXField.getText().toString() + ", " + maxYField.getText()
              .toString() + ")";
    } else {
      return "Invalid input entered into a textfield where only double values are permitted";
    }
  }

  /**
   * Loads a fractal from a file.
   *
   * @param file the file to load the fractal from
   * @return true if the fractal was loaded successfully, false otherwise
   */
  public boolean loadFractalFromFile(File file) {
    try {
      ChaosGameDescription description = fileHandler.readFromFile(file.getPath());
      long count = description.getTransforms().stream()
          .filter(JuliaTransform.class::isInstance)
          .count();
      if (count == 1) {
        setJuliaDescription(description);
        LOGGER.info("Loaded Julia set from file");
      } else {
        setGameDescription(description);
        LOGGER.info("Loaded normal game from file");
      }
      return true;
    } catch (Exception e) {
      e.printStackTrace();
      LOGGER.error("Failed to load fractal from file");
      return false;
    }
  }

  /**
   * Saves the current fractal to a file.
   *
   * @param file the file to save the fractal to
   * @return true if the fractal was saved successfully, false otherwise
   */
  public boolean saveFractalToFile(File file) {
    try {
      ChaosGameDescription description = chaosGame.getDescription();
      fileHandler.writeToFile(description, file.getPath());
      LOGGER.info("Saved fractal to file");
      return true;
    } catch (IOException e) {
      e.printStackTrace();
      LOGGER.error("Failed to save fractal to file");
      return false;
    }
  }

}

