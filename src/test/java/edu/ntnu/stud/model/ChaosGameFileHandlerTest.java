package edu.ntnu.stud.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


import edu.ntnu.stud.exceptions.EmptyFileException;
import edu.ntnu.stud.exceptions.FileNotFoundCustomException;
import edu.ntnu.stud.exceptions.IncorrectFileFormatException;
import edu.ntnu.stud.exceptions.InvalidTransformTypeException;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescription;
import edu.ntnu.stud.model.chaos.transformations.AffineTransform2D;
import edu.ntnu.stud.model.linalg.Complex;
import edu.ntnu.stud.model.chaos.transformations.JuliaTransform;
import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.chaos.transformations.Transform2D;
import edu.ntnu.stud.model.linalg.Vector2D;

import edu.ntnu.stud.model.filehandling.ChaosGameFileHandler;
import edu.ntnu.stud.model.filehandling.ChaosGameFileHandlerStrategy;
import edu.ntnu.stud.model.filehandling.TxtChaosGameFileHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.*;

public class ChaosGameFileHandlerTest {

  ChaosGameFileHandlerStrategy strategy;
  ChaosGameFileHandler fileHandler;
  String pathAffine;
  String pathJulia;

  @BeforeEach
  void setUp() {
    strategy = new TxtChaosGameFileHandler();
    fileHandler = new ChaosGameFileHandler(strategy);
    pathAffine = "src/test/resources/AffineTest.txt";
    pathJulia = "src/test/resources/JuliaTest.txt";
  }

  @Nested
  class PositiveTests {

    @Test
    @DisplayName("Test that the readFromFile method reads the correct ChaosGameDescription object")
    void testReadAffine() throws FileNotFoundCustomException {
      List<Transform2D> transform2DList = new ArrayList<>();
      AffineTransform2D transform1 = new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5),
          new Vector2D(0, 0));
      AffineTransform2D transform2 = new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5),
          new Vector2D(0.25, 0.5));
      transform2DList.add(transform1);
      transform2DList.add(transform2);
      ChaosGameDescription description = new ChaosGameDescription(new Vector2D(0, 0),
          new Vector2D(1, 1), transform2DList);
      assertEquals(true, fileHandler.readFromFile(pathAffine).equals(description));
    }

    @Test
    @DisplayName("Test that the readFromFile method reads the correct ChaosGameDescription object")
    void testReadJulia() throws FileNotFoundCustomException {
      List<Transform2D> transform2DList = new ArrayList<>();
      JuliaTransform transform1 = new JuliaTransform(new Complex(-.74543, .11301), 1);
      JuliaTransform transform2 = new JuliaTransform(new Complex(-.74543, .11301), -1);
      transform2DList.add(transform1);
      transform2DList.add(transform2);
      ChaosGameDescription description = new ChaosGameDescription(new Vector2D(-1.6, -1),
          new Vector2D(1.6, 1), transform2DList);
      assertEquals(true, fileHandler.readFromFile(pathJulia).equals(description));
    }
    @DisplayName("Test that write to affine file works as expected")
    @Test
    void testWriteAffine() throws IOException {
      ChaosGameDescription description = fileHandler.readFromFile(pathAffine);
      fileHandler.writeToFile(description, "src/test/resources/AffineWriteTest.txt");
      ChaosGameDescription newDescription = fileHandler.readFromFile(
          "src/test/resources/AffineWriteTest.txt");
      assertEquals(description, newDescription);
    }

        @DisplayName("Test that write to julia file works as expected")
    @Test
    void testWriteJulia() throws IOException {
      ChaosGameDescription description = fileHandler.readFromFile(pathJulia);
      fileHandler.writeToFile(description, "src/test/resources/JuliaWriteTest.txt");
      ChaosGameDescription newDescription = fileHandler.readFromFile(
          "src/test/resources/JuliaWriteTest.txt");
      assertEquals(description, newDescription);
    }
  }

  @DisplayName("Negative tests for ChaosGameFileHandler")
  @Nested
  class NegativeTests {

    @DisplayName("Test throws file not found exception")
    @Test
    void testFileNotFound() {
      assertThrows(FileNotFoundCustomException.class,
          () -> fileHandler.readFromFile("src/test/resources/NonExistentFile.txt"));
    }

    @DisplayName("Test incorrect file format exception")
    @Test
    void testIncorrectFileFormat() {
      assertThrows(IncorrectFileFormatException.class,
          () -> fileHandler.readFromFile("src/test/resources/IncorrectFileFormat.txt"));
    }

    @DisplayName("Test invalid transform type throws exception")
    @Test
    void testInvalidTransformType() {
      assertThrows(InvalidTransformTypeException.class,
          () -> fileHandler.readFromFile("src/test/resources/InvalidTransformType.txt"));
    }

    @DisplayName("Test read empty file throws exception")
    @Test
    void testEmptyFile() {
      assertThrows(EmptyFileException.class,
          () -> fileHandler.readFromFile("src/test/resources/EmptyFile.txt"));
    }

    @DisplayName("Test write to invalid path throws exception")
    @Test
    void testWriteInvalidPath() throws FileNotFoundCustomException {
      ChaosGameDescription description = fileHandler.readFromFile(pathAffine);
      assertThrows(IOException.class,
          () -> fileHandler.writeToFile(description, "abc/def/ghi.txt"));
    }

  }

}
