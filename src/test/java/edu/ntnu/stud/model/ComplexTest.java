package edu.ntnu.stud.model;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.model.linalg.Complex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;

public class ComplexTest {

  private Complex positiveRealPart;
  private Complex negativeRealPart;
  private Complex PosReZeroIm;
  private Complex NegReZeroIm;


  @BeforeEach
  void init() {
    positiveRealPart = new Complex(0.1, -0.4);
    negativeRealPart = new Complex(-0.1, -0.4);
    PosReZeroIm = new Complex(3, 0);
    NegReZeroIm = new Complex(-3, 0);

  }


  @DisplayName("Positive tests for Complex")
  @Nested
  class sqrtTests {

    @DisplayName("Test positive real part of complex number works")
    @Test
    void PositiveRealPart() {
      Complex sqrt = positiveRealPart.sqrt();
      assertEquals(0.506, sqrt.getX0(), 0.001);
      assertEquals(-0.395, sqrt.getX1(), 0.001);
    }

    @DisplayName("Test negative real part of complex number works")
    @Test
    void NegativeRealPart() {
      Complex sqrt = negativeRealPart.sqrt();
      assertEquals(0.395, sqrt.getX0(), 0.001);
      assertEquals(-0.506, sqrt.getX1(), 0.001);
    }

    @DisplayName("Test positive real part and zero imaginary part of complex number works")
    @Test
    void PositiveRealPartZeroImaginaryPart() {
      Complex sqrt = PosReZeroIm.sqrt();
      assertEquals(1.732, sqrt.getX0(), 0.001);
      assertEquals(0, sqrt.getX1(), 0.001);
    }

    @DisplayName("Test negative real part and zero imaginary part of complex number works")
    @Test
    void NegativeRealPartZeroImaginaryPart() {
      Complex sqrt = NegReZeroIm.sqrt();
      assertEquals(0, sqrt.getX0(), 0.001);
      assertEquals(1.732, sqrt.getX1(), 0.001);
    }
  }

}
