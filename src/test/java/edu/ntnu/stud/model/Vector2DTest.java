package edu.ntnu.stud.model;

import edu.ntnu.stud.model.linalg.Complex;
import edu.ntnu.stud.model.linalg.Vector2D;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class Vector2DTest {

  private Vector2D vector1;
  private Vector2D vector2;
  private Vector2D vector3;
  private Vector2D complex1;

  @BeforeEach
  public void init() {
    vector1 = new Vector2D(8, 3);
    vector2 = new Vector2D(3, 5);
    vector3 = new Vector2D(3, 5);
    complex1 = new Complex(3, 5);
  }

  @DisplayName("Test that the add method works as expected")
  @Test
  void testAdd() {
    Vector2D result = vector1.add(vector2);
    assertEquals(result, new Vector2D(11, 8));
  }

  @DisplayName("Test that the subtract method works as expected")
  @Test
  void testSubtract() {
    Vector2D subtractedVector = vector1.subtract(vector2);

    assertEquals(subtractedVector, new Vector2D(5, -2));
  }

  @DisplayName("Test that equals works as expected")
  @Test
  void testEquals() {
    assertNotEquals(null, vector1);
    assertNotEquals(vector2, complex1);
    assertEquals(vector2, vector3);
    assertEquals(vector1, vector1);
  }

}
