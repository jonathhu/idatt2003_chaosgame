package edu.ntnu.stud.model;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class Matrix2x2Test {

  private Matrix2x2 matrix;
  private Vector2D vector;

  @BeforeEach
  void setUp() {
    matrix = new Matrix2x2(0.5, 1, 1, 0.5);
    vector = new Vector2D(1, 2);
  }

  @DisplayName("Test that matrix multiplied with vector returns correct result")
  @Test
  void testMatrixMultiply() {
    Vector2D result = matrix.multiply(vector);
    assertEquals(2.5, result.getX0(), 0.001);
    assertEquals(2, result.getX1(), 0.001);
  }
}
