package edu.ntnu.stud.model;

import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescription;
import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescriptionFactory;
import edu.ntnu.stud.model.linalg.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ChaosGameTest {

  private ChaosGame chaosGame;
  private ChaosGame chaosGameSpy;

  @BeforeEach
  @DisplayName("Setting up the tests with a ChaosGame object")
  void setUp() {
    ChaosGameDescription description = new ChaosGameDescription(new Vector2D(0, 0),
        new Vector2D(1, 1), null);
    chaosGame = new ChaosGame(description, 100, 100);
    chaosGameSpy = spy(chaosGame);
  }

  @Nested
  class PositiveTests {

    @Test
    @DisplayName("Testing the constructor with positive parameters, to see if it creates a " +
        "ChaosGame object")
    void testConstructor() {
      assertNotNull(chaosGame);
    }

    @Test
    @DisplayName("The constructor initializes the currentPoint to (0, 0), and the steps to 0. " +
        "Testing if the currentPoint is (0, 0) and the steps is 50000")
    void testCurrentPoint() {
      ChaosGameDescription description = new ChaosGameDescription(new Vector2D(0, 0),
          new Vector2D(100, 100), null);
      ChaosGame chaosGame = new ChaosGame(description, 100, 100);
      assertEquals(new Vector2D(0, 0), chaosGame.getCurrentPoint());
      assertEquals(50000, chaosGame.getSteps());
    }

    @Test
    @DisplayName("Testing that the ChaosGame object has a ChaosCanvas object after " +
        "instantiation")
    void testCanvas() {
      assertNotNull(chaosGame.getCanvas());
    }

    @Test
    @DisplayName("Testing the setStepsAmount method with a positive number, to see if the steps " +
        "is set to the given number. Utilizing a mock object to avoid running runSteps() after " +
        "setting the steps, as that happens in the actual method")
    void testSetStepsAmount() {
      ChaosGame chaosGameSpy = spy(chaosGame);
      doNothing().when(chaosGameSpy).runSteps();
      doNothing().when(chaosGameSpy).updateCanvasObservers();
      chaosGameSpy.setStepsAmount(100);
      assertEquals(100, chaosGameSpy.getSteps());
    }

    @Test
    @DisplayName("Set Description method with a valid ChaosGameDescription object, to see if the " +
        "description is set to the given object. Using a mock object to avoid running runSteps() " +
        "after setting the description, as that happens in the actual method")
    void testSetDescription() {
      ChaosGameDescription description2 = new ChaosGameDescription(new Vector2D(0, 0),
          new Vector2D(200, 200), null);
      chaosGameSpy = spy(chaosGame);
      doNothing().when(chaosGameSpy).runSteps();
      doNothing().when(chaosGameSpy).updateCanvasObservers();
      chaosGameSpy.setDescription(description2);
      assertEquals(description2, chaosGameSpy.getDescription());
    }

    @Test
    @DisplayName("Testing the runSteps method to verify that the canvas has pixels after running " +
        "the method, by utilizing the hasPixels() method from the ChaosCanvas class")
    void testRunSteps() {
      ChaosGameDescription description = ChaosGameDescriptionFactory.createBarnsleyFern();
      chaosGame.setDescription(description);
      chaosGame.setStepsAmount(1000);
      chaosGame.runSteps();
      assertTrue(chaosGame.getCanvas().hasPixels());
    }

    @Test
    @DisplayName("Testing that the resetCurrentPoint method sets the currentPoint to (0, 0)")
    void resetCurrentPoint() {
      ChaosGameDescription description = ChaosGameDescriptionFactory.createBarnsleyFern();
      chaosGame.setDescription(description);
      chaosGame.setStepsAmount(1000);
      chaosGame.resetCurrentPoint();
      assertEquals(new Vector2D(0, 0), chaosGame.getCurrentPoint());
    }

    @Nested
    class NegativeTests {

      @Test
      @DisplayName("Testing the setStepsAmount method with a number less than 0, to see if the " +
          "steps is not set to the given number. Utilizing a mock object to avoid running runSteps() "
          + "after setting the steps, as that happens in the actual method")
      void testSetStepsAmount() {
        ChaosGame chaosGameSpy = spy(chaosGame);
        doNothing().when(chaosGameSpy).runSteps();
        doNothing().when(chaosGameSpy).updateCanvasObservers();
        chaosGameSpy.setStepsAmount(-100);
        assertNotEquals(-100, chaosGameSpy.getSteps());
      }

      @Test
      @DisplayName("Testing the runSteps method to verify that the canvas has no pixels after " +
          "running the method, by utilizing the hasPixels() method from the ChaosCanvas class")
      void testRunSteps() {
        ChaosGameDescription description = ChaosGameDescriptionFactory.createBarnsleyFern();
        ChaosGame chaosGameSpy = spy(chaosGame);
        doNothing().when(chaosGameSpy).updateCanvasObservers();
        chaosGameSpy.setDescription(description);
        chaosGameSpy.setStepsAmount(-1000);
        chaosGameSpy.runSteps();
        assertFalse(chaosGame.getCanvas().hasPixels());
      }
    }
  }
}






