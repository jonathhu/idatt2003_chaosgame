package edu.ntnu.stud.model.chaos.fractalgen;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import edu.ntnu.stud.model.chaos.fractalgen.ChaosGameDescription;
import edu.ntnu.stud.model.chaos.transformations.Transform2D;
import edu.ntnu.stud.model.chaos.transformations.AffineTransform2D;
import edu.ntnu.stud.model.linalg.Complex;
import edu.ntnu.stud.model.chaos.transformations.JuliaTransform;
import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Vector2D;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ChaosGameDescriptionTest {

  private static ChaosGameDescription chaosGameDescription1;
  private static ChaosGameDescription chaosGameDescription2;

  private static Vector2D min;
  private static Vector2D max;

  private static Transform2D affineTransform1;
  private static Transform2D affineTransform2;
  private static Transform2D juliaTransform1;
  private static Transform2D juliaTransform2;

  @BeforeAll
  static void setUpAffine() {
    affineTransform1 = new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0));
    affineTransform2 = new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5),
        new Vector2D(0.25, 0.5));
    min = new Vector2D(0, 0);
    max = new Vector2D(1, 1);

    List<Transform2D> transform2DList = new ArrayList<>();
    transform2DList.add(affineTransform1);
    transform2DList.add(affineTransform2);

    chaosGameDescription1 = new ChaosGameDescription(min, max, transform2DList);
  }

  @BeforeAll
  static void setUpJulia() {
    juliaTransform2 = new JuliaTransform(new Complex(-.74543, .11301), 1);
    juliaTransform1 = new JuliaTransform(new Complex(-.74543, .11301), -1);

    List<Transform2D> transform2DList = new ArrayList<>();
    transform2DList.add(juliaTransform1);
    transform2DList.add(juliaTransform2);

    chaosGameDescription2 = new ChaosGameDescription(min, max, transform2DList);
  }

  @DisplayName("Positive tests for ChaosGameDescription")
  @Nested
  class PositiveTests {

    @DisplayName("Test that same ChaosGameDescription objects are equal")
    @Test
    void testTrueEquals() {
      ChaosGameDescription chaosGameDescription = new ChaosGameDescription(min, max,
          List.of(affineTransform1, affineTransform2));
      assertEquals(chaosGameDescription1, chaosGameDescription);
    }
  }

  @DisplayName("Negative tests for ChaosGameDescription")
  @Nested
  class NegativeTests {

    @DisplayName("Test that different ChaosGameDescription objects are not equal")
    @Test
    void testFalseEquals() {
      ChaosGameDescription chaosGameDescription = new ChaosGameDescription(min, max,
          List.of(juliaTransform1, juliaTransform2));
      assertNotEquals(chaosGameDescription1, chaosGameDescription);
    }
  }
}
