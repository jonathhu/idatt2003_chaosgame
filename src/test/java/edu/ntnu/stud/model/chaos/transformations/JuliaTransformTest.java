package edu.ntnu.stud.model.chaos.transformations;

import edu.ntnu.stud.model.linalg.Complex;
import edu.ntnu.stud.model.linalg.Vector2D;
import edu.ntnu.stud.model.chaos.transformations.JuliaTransform;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class JuliaTransformTest {

  private JuliaTransform juliaTransform;

  private Complex constantPoint;
  private int sign;
  private Vector2D pointToTransform;

  @BeforeEach
  public void init() {
    constantPoint = new Complex(0.3, 0.6);
    sign = 1;
    juliaTransform = new JuliaTransform(constantPoint, sign);
    pointToTransform = new Vector2D(0.4, 0.2);
  }

  @Nested
  @DisplayName("Positive tests for JuliaTransform")
  class PositiveTests {

    @Test
    @DisplayName("Test that the transform method works as expected")
    void positiveTransform() {
      Vector2D transformedPoint = juliaTransform.transform(pointToTransform);
      assertEquals(transformedPoint.getX0(), 0.506, 0.001);
      assertEquals(transformedPoint.getX1(), -0.395, 0.001);
    }

    @Test
    @DisplayName("Test that the equals method works as expected")
    void positiveEquals() {
      JuliaTransform juliaTransform2 = new JuliaTransform(constantPoint, sign);
      assertEquals(juliaTransform, juliaTransform2);
    }
  }

  @Nested
  @DisplayName("Negative tests for JuliaTransform")
  class NegativeTests {

    @Test
    @DisplayName("Test that transform method is not always positive")
    void negativeTransform() {
      Vector2D transformedPoint = juliaTransform.transform(pointToTransform);
      assertNotEquals(transformedPoint.getX0(), 1);
      assertNotEquals(transformedPoint.getX1(), -1);
    }

    @Test
    @DisplayName("Test that the equals method returns false when comparing two different JuliaTransform objects")
    void negativeEquals() {
      JuliaTransform juliaTransform2 = new JuliaTransform(new Complex(0.3, 0.6), -1);
      assertNotEquals(juliaTransform, juliaTransform2);
    }
  }
}
