package edu.ntnu.stud.model.chaos.fractalgen;

import edu.ntnu.stud.model.chaos.fractalgen.ChaosCanvas;
import edu.ntnu.stud.model.linalg.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ChaosCanvasTest {

  private ChaosCanvas canvas;
  private Vector2D point;

  @BeforeEach
  void setUp() {

    canvas = new ChaosCanvas(1400, 800, new Vector2D(0, 0), new Vector2D(1, 1));
    point = new Vector2D(0.5, 0.5);
  }

  @DisplayName("Positive tests for ChaosCanvas")
  @Nested
  class PositiveTests {

    @DisplayName("Test that putPixel and getPixel works")
    @Test
    void testPutAndGetPixel() {
      canvas.putPixel(point);
      assertEquals(canvas.getPixel(point), 1);
      assertEquals(canvas.getPixel(new Vector2D(1, 0.5)), 0);
    }

    @DisplayName("Test that canvas is cleared correctly")
    @Test
    void testClearCanvas() {
      canvas.putPixel(point);
      canvas.clear();
      assertEquals(canvas.getPixel(point), 0);
    }

    @DisplayName("Test the hasPixels method to verify that it returns true when canvas has pixels.")
    @Test
    void testHasPixelsTrue() {
      canvas.putPixel(point);
      assertTrue(canvas.hasPixels());
    }

    @DisplayName("Test the hasPixels method to verify that it returns false when canvas is empty.")
    @Test
    void testHasPixelsFalse() {
      assertFalse(canvas.hasPixels());
    }

    @DisplayName("Test the getCanvasArray method to verify that it returns the correct canvas "
        + "array with the height and width of the canvas.")
    @Test
    void testGetCanvasArray() {
      int[][] canvasArray = canvas.getCanvasArray();
      assertEquals(canvas.getHeight(), canvasArray.length);
      assertEquals(canvas.getWidth(), canvasArray[0].length);
    }
  }

  @DisplayName("Negative tests for ChaosCanvas")
  @Nested
  class NegativeTests {

    @DisplayName("Test that accessing canvas array outside its dimensions throws exception")
    @Test
    void testAccessCanvasArrayOutsideDimensions() {
      int[][] canvasArray = canvas.getCanvasArray();
      assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
        int pixel = canvasArray[canvas.getHeight()][canvas.getWidth()];
      });
    }

    @DisplayName("Test that getting pixel outside canvas throws exception")
    @Test
    void testGetPixelOutsideCanvas() {
      Vector2D pointOutside = new Vector2D(2, 2);
      assertThrows(ArrayIndexOutOfBoundsException.class, () -> canvas.getPixel(pointOutside));
    }

    @DisplayName("Test that invalid width input as constructor parameter throws exception")
    @Test
    void testInvalidWidth() {
      assertThrows(NegativeArraySizeException.class,
          () -> new ChaosCanvas(-1, 800, new Vector2D(0, 0), new Vector2D(1, 1)));
    }

    @DisplayName("Test that invalid height input as constructor parameter throws exception")
    @Test
    void testInvalidHeight() {
      assertThrows(NegativeArraySizeException.class,
          () -> new ChaosCanvas(1400, -1, new Vector2D(0, 0), new Vector2D(1, 1)));
    }

    @DisplayName("Test that invalid min and max coordinates as constructor parameters throws exception")
    @Test
    void testInvalidMinMaxCoords() {
      assertThrows(IllegalArgumentException.class,
          () -> new ChaosCanvas(1400, 800, new Vector2D(1, 1), new Vector2D(0, 0)));
    }

    @DisplayName("Test that if min and max coordinates are equal throws exception")
    @Test
    void testMinMaxCoordsAreEqual() {
      assertThrows(IllegalArgumentException.class,
          () -> new ChaosCanvas(1400, 800, new Vector2D(1, 1), new Vector2D(1, 1)));
    }

  }
}