package edu.ntnu.stud.model.chaos.transformations;

import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Vector2D;
import edu.ntnu.stud.model.chaos.transformations.AffineTransform2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AffineTransform2DTest {

  private AffineTransform2D transform;
  private Matrix2x2 matrix;
  private Vector2D vector;
  private Vector2D point;

  @BeforeEach
  void setUp() {
    matrix = new Matrix2x2(0.5, 1, 1, 0.5);
    vector = new Vector2D(3, 1);
    transform = new AffineTransform2D(matrix, vector);
    point = new Vector2D(1, 2);
  }

  @Nested
  @DisplayName("Positive tests for AffineTransform2D")
  class PositiveTests {

    @Test
    @DisplayName("Test to verify that a valid AffineTransform2D object is " +
        "created when given valid inputs")
    void positiveConstructorTest() {
      AffineTransform2D transform2 = new AffineTransform2D(matrix, vector);
      assertNotNull(transform2);
    }

    @Test
    @DisplayName("Test to verify that transform method returns the correct " +
        "transformed point")
    void positiveTransformTest() {
      Vector2D transformedPoint = transform.transform(point);
      assertEquals(new Vector2D(5.5, 3), transformedPoint);
    }

    @Test
    @DisplayName("Test to verify that the equals method returns true when " +
        "comparing two equal AffineTransform2D objects")
    void positiveEqualsTest() {
      AffineTransform2D transform2 = new AffineTransform2D(matrix, vector);
      assertEquals(transform, transform2);
    }
  }

  @Nested
  @DisplayName("Negative tests for AffineTransform2D")
  class NegativeTests {

    @Test
    @DisplayName("Test that transform method is not always positive")
    void negativeTransformTest() {
      Vector2D transformedPoint = transform.transform(point);
      assertNotEquals(new Vector2D(1, 1), transformedPoint);
    }

    @Test
    @DisplayName("Test that the equals method returns false when comparing " +
        "two different AffineTransform2D objects")
    void negativeEqualsTest() {
      AffineTransform2D transform2 = new AffineTransform2D(new Matrix2x2(1, 0, 0, 1),
          new Vector2D(0, 0));
      assertNotEquals(transform, transform2);
    }
  }

}