# IDATT2003 - Chaos Game

###### Contributer 1
STUDENT NAME = Jonathan Hübertz  
STUDENT ID = 10117

###### Contributer 2
STUDENT NAME = Aryan Malekian  
STUDENT ID = 10015


## Project Description
The project's description was to develop a JavaFX desktop application
where the user can visualize fractals in a graphical user interface.  

The requirements for the application were as follows:
- The user should be able to choose between different predefined fractals.
- The user should be able to create their own fractals.
- The user should be able to edit the values of the fractals.
- The user should be able to save the configurations of the fractals to file.
- The user should be able to load the configurations of the fractals from file.
- The user should be able to visualize the fractals in the graphical user interface.


## Project Structure
The project is set up as a maven project. The project is divided into two main folders:
- The main folder, which contains the main code of the project
- The test folder, which contains the tests of the project

Each of these folders are divided into 2 subfolders:
- The java folder, which contains the java code.
- The resources folder, which contains the resources of the project

The java folder is separated into different packages and is designed in relation to the model-view-controller pattern.
- The **model** package contains the classes regarding the logic of how fractals are calculated and their behaviours.
- The **view** package contains the logic for user-interaction and the graphical user interface.
- The **controller** package contains the logic for the communication between the model and the view.

There are other packages in the java folder as well, such as exceptions which contains custom exceptions, observer which contains the interfaces for the observer pattern, and util which contains utility classes.


## How to run the project
#### Pre-requisites
- JDK is installed and JAVA_HOME points to the JDK installation directory
- Maven is installed and MAVEN_HOME points to the Maven installation directory

#### Running the project
**1. Verify that the pre-requisites are met:**
Open a terminal and run the following commands:
```zsh
java -version
mvn -version
```

**2. Clone the repository**  
Open a terminal and run the following command:  
With *HTTPS*
```zsh
git clone https://gitlab.stud.idi.ntnu.no/jonathhu/idatt2003_chaosgame.git
```

With *SSH*
```zsh
git clone git@gitlab.stud.idi.ntnu.no:jonathhu/idatt2003_chaosgame.git
```


**3. Verify that you're in the right directory**
If you run the following command:
```zsh
ls -la
```
the pom.xml file should be visible in the output.  
If it is not, you are in the wrong directory and need to navigate to it.

**4. Run the project**
To run the project through an IDE, simply run the Main.java class.
To run the project from the terminal run the following command:
```zsh  
mvn javafx:run
```
To run the tests for the project run the following command:
```zsh
mvn clean test
```

## Link to repository
GitLab repository: https://gitlab.stud.idi.ntnu.no/jonathhu/idatt2003_chaosgame.git